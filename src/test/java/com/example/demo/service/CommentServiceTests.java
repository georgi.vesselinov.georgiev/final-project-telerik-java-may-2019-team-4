package com.example.demo.service;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Post;
import com.example.demo.models.Reply;
import com.example.demo.models.User;
import com.example.demo.repositories.CommentRepository;
import com.example.demo.repositories.ReplyRepository;
import com.example.demo.services.CommentServiceImpl;
import com.example.demo.services.ReplyServiceImpl;
import com.example.demo.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTests {


    @Mock
    CommentRepository mockCommentRepository;

    @Mock
    ReplyRepository mockReplyRepository;

    @InjectMocks
    CommentServiceImpl commentService;


    @InjectMocks
    UserServiceImpl userService;

    @InjectMocks
    ReplyServiceImpl replyService;

    @Before


    @Test
    public void get_CommentById_Should_Return_A_Comment_When_Id_Matches(){

        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        Comment commentForTest = commentService.getCommentById(1);

        // Assert
        Assert.assertEquals("commentText", commentForTest.getCommentText());
    }


    @Test(expected = EntityNotFoundException.class)
    public void get_CommentById_Should_Throw_An_Exception_When_Id_NotExists(){

        // Act
        Comment commentForTest = commentService.getCommentById(2);
    }

    @Test
    public void get_AllComments_Should_Return_A_List_Of_Comments() {

        //Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Comment comment2=new Comment(post,user,"commentText2");


        Mockito.when(mockCommentRepository.findAll())
                .thenReturn(Arrays.asList(comment,comment2));
        //Act

        List<Comment> comments=commentService.getAllComments();

        //Assert
        Assert.assertEquals(2, comments.size());
    }


    @Test
    public void deleteComment_Should_Invoke_Repository_Delete() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply1 =new Reply("replyText1",comment,user);
        Reply reply2 =new Reply("replyText2",comment,user);
        Reply reply3 =new Reply("replyText3",comment,user);


        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.deleteComment(1);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).deleteById(1);
        Assert.assertEquals(0, commentService.getAllComments().size());

    }



    @Test
    public void createComment_Should_Invoke_Repository_Save() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");

        // Act
        commentService.createComment(comment);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).save(comment);
        Assert.assertEquals("commentText", commentService.getCommentById(1).getCommentText());

    }


    @Test(expected = EntityBadRequestException.class)
    public void createComment_Should_Throw_An_Exception_When_TextEmpty() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"");

        // Act
        commentService.createComment(comment);

    }


    @Test
    public void updateComment_Should_Invoke_Repository_Save() {

        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        CommentDTO commentDTO =new CommentDTO("newCommentText");
        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.updateComment(1,commentDTO);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).save(comment);
        Assert.assertEquals("newCommentText", comment.getCommentText());
    }


    @Test(expected = EntityBadRequestException.class)
    public void updateComment_Should_Throw_An_Exception_When_TextEmpty(){

        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply1 =new Reply("",comment,user);
        CommentDTO commentDTO =new CommentDTO();
        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));
        // Act
        commentService.updateComment(1,commentDTO);
    }



    @Test
    public void addCommentLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");

        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.addCommentLike(1,user);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).save(comment);
        Assert.assertEquals(1, comment.getCommentLikes().size());
    }



    @Test(expected = EntityAlreadyExistsException.class)
    public void addCommentLike_Should_Throw_An_Exception_When_CommentLikeExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        comment.getCommentLikes().add(user);

        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.addCommentLike(1,user);
    }

    @Test
    public void removeCommentLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Reply reply =new Reply("replyText",comment,user);
        comment.getCommentLikes().add(user);

        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.removeCommentLike(1,user);
        // Assert
        Mockito.verify(mockCommentRepository, Mockito.times(1)).save(comment);
        Assert.assertEquals(0, comment.getCommentLikes().size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeCommentLike_Throws_An_Exception_When_CommentLikeExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");

        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.removeCommentLike(1,user);
    }


    @Test
    public void addRemoveAndCountCommentLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Comment comment=new Comment(post,user,"commentText");
        Mockito.when(mockCommentRepository.findById(1))
                .thenReturn(Optional.of(comment));

        // Act
        commentService.addRemoveAndCountCommentLike(1,user);
        commentService.addRemoveAndCountCommentLike(1,user);
        // Assert

        Mockito.verify(mockCommentRepository, Mockito.times(2)).save(comment);
        Assert.assertEquals(0, comment.getCommentLikes().size());
    }


    @Test
    public void generateCommentFromDTO_Should_Return_A_Comment() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        user.setId(1);
        CommentDTO commentDTO=new CommentDTO("commentDTOText");

        // Act
        Comment comment=commentService.generateCommentFromDTO(post,user,commentDTO);
        // Assert
        Assert.assertEquals("commentDTOText", comment.getCommentText());
    }


    @Test(expected = EntityBadRequestException.class)
    public void generateCommentFromDTO_Throws_An_Exception_When_CommentDTOText_NotExists() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        user.setId(1);
        CommentDTO commentDTO=new CommentDTO();

        // Act
        Comment comment=commentService.generateCommentFromDTO(post,user,commentDTO);
      }

}
