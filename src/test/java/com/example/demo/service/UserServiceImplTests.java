package com.example.demo.service;

import com.example.demo.exceptions.*;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.DTO.UserDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockUserRepository;

    @InjectMocks
    UserServiceImpl userService;

    private User user;
    private List<User> userList;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        user = Mockito.mock(User.class);
        userList = new ArrayList<>();
        userList.add(user);
        when(mockUserRepository.findAllByEnabledTrue()).thenReturn(userList);
    }

    @Test
    public void get_all_should_return_all_users() {
        //Act
        List<User> result = userService.getAll();
        //Assert
        Assert.assertEquals(userList, result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void get_user_by_id_should_throw_exception_when_user_not_exists() {
        //Arrange
        when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.empty());
        //Act
        userService.getUserById(1);
    }

    @Test
    public void get_user_by_id_should_return_user_with_valid_id() {
        //Arrange
        when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user));
        //Act
        User result = userService.getUserById(1);
        //Assert
        Assert.assertEquals(Optional.of(user).get(), result);
    }

    @Test
    public void get_user_by_username_should_return_user_with_valid_username() {
        //Arrange
        when(mockUserRepository.findByEmailAndEnabledTrue(anyString())).thenReturn(Optional.ofNullable(user));
        //Act
        User result = userService.getUserByUserName("username");
        //Assert
        Assert.assertEquals(Optional.of(user).get(), result);
    }

    @Test(expected = EntityNotFoundException.class)
    public void get_user_by_username_throws_exception_when_notPresent() {

        //Act
        User result = userService.getUserByUserName("username");
    }

    @Test(expected = EntityAlreadyDisabledException.class)
    public void deleteUser_should_throwException_user_when_account_is_disabled() {
        //Arrange
        when(mockUserRepository.findById(anyInt())).thenReturn(Optional.ofNullable(user));
        //Act
        userService.deleteUser(1);
        //Assert
        Assert.assertTrue(!user.isEnabled());
    }

    @Test
    public void createUser_Should_Call_Repository_Create() {
        User user = new User("123456", "gosho", "goshev", "gosho@abv.bg");
        userService.registerUser(user);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user);
    }


//    @Test
//    public void deleteUser_Should_Call_Repository_Create() {
//        User user = new User("123456", "gosho", "goshev", "gosho@abv.bg");
//        user.setEnabled(true);
//        lenient().when(mockUserRepository.findById(1)).thenReturn(Optional.of(user));
//          when(userService.getUserByIdAdmin(1)).thenReturn(user);
//        userService.deleteUser(1);
//        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(user);
//    }

    @Test(expected = EntityNotFoundException.class)
    public void deleteUser_Should_Throw_Exception_When_User_NotFound() {
        userService.deleteUser(1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).delete(user);
    }

    @Test
    public void get_AllAdmin_Should_Return_A_List_Of_Users() {

        //Arrange
        User user = new User("pass1", "fName1", "lName1", "email1");
        User user2 = new User("pass2", "fName2", "lName2", "email2");
        User user3 = new User("pass3", "fName3", "lName3", "email3");

        Mockito.when(mockUserRepository.findAll())
                .thenReturn(Arrays.asList(user, user2, user3));
        //Act

        List<User> allUsers = userService.getAllAdmin();

        //Assert
        Assert.assertEquals(3, allUsers.size());
        Assert.assertEquals("fName1", allUsers.get(0).getFirstName());

    }


    @Test
    public void saveUser_Should_Call_Repository_Save() {

        //Arrange
        User user = new User("pass1", "fName1", "lName1", "email1");
        user.setId(1);
        //Act
        userService.saveUser(user);
        //Assert

        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user);

    }


    @Test
    public void sendFriendRequest_Should_Call_Repository_Save() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findById(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findById(2)).thenReturn(Optional.of(user2));
        user1.setId(1);
        user2.setId(2);

        //Act
        userService.sendFriendRequest(user1, user2);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user2);
        Assert.assertTrue(user1.getSentFriendRequests().contains(user2));
        Assert.assertTrue(user1.getSentFriendRequests().contains(user2));

    }



    @Test(expected = EntityAlreadyExistsException.class)
    public void sendFriendRequest_Throws_Exception_When_Request_AlreadyExists() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.getSentFriendRequests().add(user2);
        user2.getReceivedFriendRequests().add(user1);
        user1.setId(1);
        user2.setId(2);

        //Act
        userService.sendFriendRequest(user1, user2);

    }

    @Test(expected = EntityAlreadyExistsException.class)
    public void sendFriendRequest_Throws_Exception_When_ReverseRequest_AlreadyExists() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user2.getSentFriendRequests().add(user1);
        user1.getReceivedFriendRequests().add(user2);
        user1.setId(1);
        user2.setId(2);

        //Act
        userService.sendFriendRequest(user1, user2);

    }


    @Test(expected = EntityAlreadyExistsException.class)
    public void sendFriendRequest_Throws_Exception_When_Friendship_AlreadyExists() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.getFriendListAsRequester().add(user2);
        user2.getFriendListAsAccepter().add(user1);
        user1.setId(1);
        user2.setId(2);

        //Act
        userService.sendFriendRequest(user1, user2);

    }

    @Test(expected = EntityBadRequestException.class)
    public void sendFriendRequest_Throws_Exception_When_Sender_And_Receiver_SamePerson() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        user1.setId(1);

        //Act
        userService.sendFriendRequest(user1, user1);

    }


    @Test
    public void rejectFriendRequest_Should_Call_Repository_Save() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.getSentFriendRequests().add(user2);
        user2.getReceivedFriendRequests().add(user1);
        user1.setId(1);
        user2.setId(2);
        //Act
        userService.rejectFriendRequest(1, 2);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user2);
        Assert.assertFalse(user1.getSentFriendRequests().contains(user2));
        Assert.assertFalse(user2.getReceivedFriendRequests().contains(user1));

    }



    @Test(expected = EntityNotFoundException.class)
    public void rejectFriendRequest_Throws_Exception_When_Request_NotExists() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        //Act
        userService.rejectFriendRequest(1, 2);
    }

    @Test
    public void acceptFriendRequest_Should_Call_Repository_Save() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.getSentFriendRequests().add(user2);
        user2.getReceivedFriendRequests().add(user1);
        user1.setId(1);
        user2.setId(2);
        //Act
        userService.acceptFriendRequest(1, 2);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user2);
        Assert.assertTrue(user1.getFriendListAsRequester().contains(user2));
        Assert.assertTrue(user2.getFriendListAsAccepter().contains(user1));

    }



    @Test
    public void deleteFriendship_Should_Call_Repository_Save() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.getFriendListAsRequester().add(user2);
        user2.getFriendListAsAccepter().add(user1);
        user1.setId(1);
        user2.setId(2);
        //Act
        userService.deleteFriendship(1, 2);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user2);
        Assert.assertFalse(user1.getFriendListAsRequester().contains(user2));
        Assert.assertFalse(user2.getFriendListAsAccepter().contains(user1));

    }



    @Test(expected = EntityNotFoundException.class)
    public void deleteFriendship_Throws_Exception_When_Request_NotExists() {
        //Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "gowwwwwsho2@abv.bg");
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(1)).thenReturn(Optional.of(user1));
        lenient().when(mockUserRepository.findByIdAndEnabledTrue(2)).thenReturn(Optional.of(user2));
        user1.setId(1);
        user2.setId(2);
        //Act
        userService.deleteFriendship(1, 2);

    }



    @Test
    public void setPrivate_Should_Invoke_Repository_Save_And_Return_CorrectData() {
        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        user1.setShared(true);
        user1.setId(1);
        Mockito.when(mockUserRepository.findByIdAndEnabledTrue(1))
                .thenReturn(Optional.of(user1));

        // Act
        userService.setPrivate(1);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Assert.assertFalse(user1.isShared());

    }



    @Test(expected = EntityAlreadyChangedException.class)
    public void setPrivate_Throws_Exception_When_Data_Matches() {
        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        user1.setShared(false);
        Mockito.when(mockUserRepository.findByIdAndEnabledTrue(1))
                .thenReturn(Optional.of(user1));


        // Act
        userService.setPrivate(1);

    }





    @Test
    public void setPublic_Should_Invoke_Repository_Save_And_Return_CorrectData() {
        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        user1.setShared(false);
        user1.setId(1);
        Mockito.when(mockUserRepository.findByIdAndEnabledTrue(1))
                .thenReturn(Optional.of(user1));

        // Act
        userService.setPublic(1);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).save(user1);
        Assert.assertTrue(user1.isShared());

    }



    @Test(expected = EntityAlreadyChangedException.class)
    public void setPublic_Throws_Exception_When_Data_Matches() {
        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        user1.setShared(true);
        Mockito.when(mockUserRepository.findByIdAndEnabledTrue(1))
                .thenReturn(Optional.of(user1));


        // Act
        userService.setPublic(1);

    }



    @Test
    public void get_AllByPage_Should_Return_A_PageOfUsers() {

        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "goshwo2@abv.bg");

        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        Page<User> pagedResponse = new PageImpl(users);
        Mockito.when(mockUserRepository.findAll(new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<User> page = userService.getAllByPage(new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("gosho", page.getContent().get(0).getFirstName());
    }



    @Test
    public void get_AllEnabledByPage_Should_Return_A_PageOfUsers() {

        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "goshwo2@abv.bg");

        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        Page<User> pagedResponse = new PageImpl(users);
        Mockito.when(mockUserRepository.findAllByEnabledTrue(new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<User> page = userService.getAllEnabledByPage(new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("gosho", page.getContent().get(0).getFirstName());
    }


    @Test
    public void updatePassword_Should_Call_Repository_Save() {

        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        user.setId(1);
//        Mockito.when(mockUserRepository.findByIdAndEnabledTrue(1))
//                .thenReturn(Optional.of(user1));
        // Act
        userService.updatePassword("pesho",1);
        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).updatePassword("pesho",1);
    }


    @Test
    public void findByName_Should_Return_A_PageOfUsers() {

        // Arrange
        User user1 = new User("123456", "gosho", "goshev", "goshwo@abv.bg");
        User user2 = new User("1234567", "gosho2", "goshev2", "goshwo2@abv.bg");

        List<User> users = new ArrayList<User>();
        users.add(user1);
        users.add(user2);
        Page<User> pagedResponse = new PageImpl(users);
        Mockito.when(mockUserRepository.findAllByEnabledTrueAndFirstNameLike("%string%",new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<User> page = userService.findByName("string",new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("gosho", page.getContent().get(0).getFirstName());
    }
}
