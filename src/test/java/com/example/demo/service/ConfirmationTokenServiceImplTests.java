package com.example.demo.service;

import com.example.demo.models.ConfirmationToken;
import com.example.demo.models.User;
import com.example.demo.repositories.ConfirmationTokenRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.ConfirmationTokenServiceImpl;
import com.example.demo.services.UserServiceImpl;
import com.example.demo.services.contracts.ConfirmationTokenService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
@RunWith(MockitoJUnitRunner.class)
public class ConfirmationTokenServiceImplTests {

    @Mock
    ConfirmationTokenRepository confirmationTokenRepository;

    @InjectMocks
    ConfirmationTokenServiceImpl confirmationTokenService;



    private ConfirmationToken confirmationToken;



    @Test
    public void get_confirmation_token_should_return_valid_token() {
        //Arrange
        when(confirmationTokenRepository.findByConfirmationToken("token")).thenReturn((confirmationToken));
        //Act
        ConfirmationToken result = confirmationTokenService.findByConfirmationToken("token");
        //Assert
        Assert.assertEquals(confirmationToken, result);
    }

    @Test
    public void saveToken_Should_save_token_in_repository() {
        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationTokenService.saveToken(confirmationToken);
        Mockito.verify(confirmationTokenRepository, Mockito.times(1)).save(confirmationToken);
    }

    @Test
    public void deleteToken_Should_delete_token_from_repository() {
        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationTokenService.deleteToken(confirmationToken);
        Mockito.verify(confirmationTokenRepository, Mockito.times(1)).delete(confirmationToken);
    }
}
