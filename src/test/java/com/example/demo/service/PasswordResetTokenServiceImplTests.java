package com.example.demo.service;

import com.example.demo.models.ConfirmationToken;
import com.example.demo.models.PasswordResetToken;
import com.example.demo.repositories.PasswordResetTokenRepository;

import com.example.demo.services.PasswordResetTokenServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PasswordResetTokenServiceImplTests {
    @Mock
    PasswordResetTokenRepository passwordResetTokenRepository;

    @InjectMocks
    PasswordResetTokenServiceImpl passwordResetTokenService;

    private PasswordResetToken passwordResetToken;


    @Test
    public void get_password_reset_token_should_return_valid_token() {
        //Arrange
        when(passwordResetTokenRepository.findByPasswordResetToken("token")).thenReturn((passwordResetToken));
        //Act
        PasswordResetToken result = passwordResetTokenService.findByPasswordResetToken("token");
        //Assert
        Assert.assertEquals(passwordResetToken, result);
    }

    @Test
    public void saveToken_Should_save_token_in_repository() {
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetTokenService.saveToken(passwordResetToken);
        Mockito.verify(passwordResetTokenRepository, Mockito.times(1)).save(passwordResetToken);
    }

    @Test
    public void deleteToken_Should_delete_token_from_repository() {
       PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetTokenService.deleteToken(passwordResetToken);
        Mockito.verify(passwordResetTokenRepository, Mockito.times(1)).delete(passwordResetToken);
    }
}
