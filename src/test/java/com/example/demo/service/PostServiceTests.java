package com.example.demo.service;

import com.example.demo.exceptions.EntityAlreadyChangedException;
import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.repositories.PostRepository;
import com.example.demo.services.PostServiceImpl;
import com.example.demo.services.contracts.PostService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceTests {

    @Mock
    PostRepository mockPostRepository;

    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void get_PostById_Should_Return_A_Post_When_Id_Matches() {

        // Arrange

        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        Post postForTest = postService.getPostById(1);

        // Assert
        Assert.assertEquals("postText", postForTest.getPostText());
    }


    @Test(expected = EntityNotFoundException.class)
    public void get_PostById_Should_Return_An_Exception_When_Id_NotExists() {

        // Arrange

//        Post post1=new Post("postText1",new User("pass1","fName1","lName1","email1"),true);
//        Post post2=new Post("postText2",new User("pass1","fName1","lName1","email1"),true);
//        Post post3=new Post("postText3",new User("pass1","fName1","lName1","email1"),true);
//        Post post4=new Post("postText4",new User("pass1","fName1","lName1","email1"),true);
//        Post post5=new Post("postText5",new User("pass1","fName1","lName1","email1"),true);
//
//        Mockito.lenient().when(mockPostRepository.findAll())
//                .thenReturn(Arrays.asList(post1,post2,post3,post4,post5));

        // Act
        Post postForTest = postService.getPostById(6);
    }


    //?????? ne e qsno ok li e
    @Test
    public void get_AllSharedByPage_Should_Return_A_PageOfPost() {

        // Arrange

        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);

        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllBySharedTrueOrderByCreatedAtDesc(new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getAllSharedByPage(new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }


    @Test
    public void createPost_Should_Invoke_Repository_Save() {
        // Arrange
        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post1));

        // Act
        postService.createPost(post1);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post1);
        Assert.assertEquals("postText1", postService.getPostById(1).getPostText());

    }


    @Test(expected = EntityBadRequestException.class)
    public void createPost_Should_Throw_Exception_When_PostEmpty() {
        // Act
        Post post1 = new Post("", new User("pass1", "fName1", "lName1", "email1"), true);

        // Arrange
        postService.createPost(post1);
    }


    @Test
    public void updatePost_Should_Invoke_Repository_Save() {
        // Arrange
        User user = new User("pass1", "fName1", "lName1", "email1");
        Post post = new Post("postText", user, true);
        PostDTO postDTO = new PostDTO("newPostText",null,false);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.updatePost(1, postDTO);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertEquals("newPostText", post.getPostText());
    }


    @Test(expected = EntityBadRequestException.class)
    public void updatePost_Should_Throw_An_Exception_When_TextEmpty() {

        // Arrange

        User user = new User("pass1", "fName1", "lName1", "email1");
        Post post = new Post("postText", user, true);
        PostDTO postDTO = new PostDTO(null,null,false);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.updatePost(1, postDTO);
    }


    @Test
    public void chancePrivacy_Should_Invoke_Repository_Save_And_Return_CorrectData() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(true);
        PostDTO postDTO = new PostDTO(null,null,false);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.chancePrivacy(1,postDTO);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertEquals(false, postService.getPostById(1).isShared());

    }



    @Test(expected = EntityAlreadyChangedException.class)
    public void chancePrivacy_Throws_Exception_When_Data_Matches() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(true);
        PostDTO postDTO = new PostDTO(null,null,true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.chancePrivacy(1,postDTO);

    }


    @Test
    public void changePostPrivacy_Throws_Exception_When_Data_Matches() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), false);
        post2.setPostId(2);
        post.setPostId(1);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));
        Mockito.when(mockPostRepository.findById(2))
                .thenReturn(Optional.of(post2));

        // Act
        postService.changePostPrivacy(1);
        postService.changePostPrivacy(2);
        // Assert
        Assert.assertFalse(post.isShared());
        Assert.assertTrue(post2.isShared());

    }



    @Test
    public void setPrivate_Should_Invoke_Repository_Save_And_Return_CorrectData() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.setPrivate(1);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertFalse(postService.getPostById(1).isShared());

    }



    @Test(expected = EntityAlreadyChangedException.class)
    public void setPrivate_Throws_Exception_When_Data_Matches() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(false);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.setPrivate(1);

    }





    @Test
    public void setPublic_Should_Invoke_Repository_Save_And_Return_CorrectData() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(false);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.setPublic(1);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertTrue (postService.getPostById(1).isShared());

    }



    @Test(expected = EntityAlreadyChangedException.class)
    public void setPublic_Throws_Exception_When_Data_Matches() {
        // Arrange
        Post post = new Post("postText", new User("pass1", "fName1", "lName1", "email1"), true);
        post.setShared(true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.setPublic(1);

    }


    @Test
    public void addPostLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.addPostLike(1,user);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertEquals(1, post.getPostLikes().size());
    }

    @Test(expected = EntityAlreadyExistsException.class)
    public void addPostLike_Should_Throw_An_Exception_When_PostLikeExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        post.getPostLikes().add(user);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.addPostLike(1,user);
    }




    @Test
    public void removePostLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        post.getPostLikes().add(user);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.removePostLike(1,user);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).save(post);
        Assert.assertEquals(0, post.getPostLikes().size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void removePostLike_Should_Throw_An_Exception_When_PostLikeNotExists() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.removePostLike(1,user);
    }



    @Test
    public void deletePost_Should_Invoke_Repository_Delete() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.deletePost(1);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(1)).deleteById(1);
     }



    @Test(expected = EntityNotFoundException.class)
    public void deletePost_Throws_An_Exception_When_PostNotExists() {
        // Arrange

        // Act
        postService.deletePost(2);
    }


    @Test
    public void addRemoveAndCountPostLike_Should_Invoke_Repository_Save_And_Give_CorrectCount() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        Post post=new Post("postText",user,true);
        Mockito.when(mockPostRepository.findById(1))
                .thenReturn(Optional.of(post));

        // Act
        postService.addRemoveAndCountPostLike(1,user);
        postService.addRemoveAndCountPostLike(1,user);
        // Assert
        Mockito.verify(mockPostRepository, Mockito.times(2)).save(post);
        Assert.assertEquals(0, post.getPostLikes().size());
    }


    @Test
    public void get_MostPopularOnes_Should_Return_A_PageOfPost() {

        // Arrange

        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);

        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllBySharedTrueOrderByCommentsDesc(new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getMostPopularOnes(new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }

    @Test
    public void get_AllPostsPageable_Should_Return_A_PageOfPost() {

        // Arrange

        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);

        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllByOrderByCreatedAtDesc(new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getAllPostsPageable(new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }


    @Test
    public void get_sharedPostsOfNonFriend_Should_Return_A_PageOfPost() {

        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        user.setId(1);
        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);


        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllByUserAndSharedIsTrueOrderByCreatedAtDesc(user,new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getSharedPostsOfNonFriend(user,new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }


    @Test
    public void getMyUserFriendPosts_Should_Return_A_PageOfPost() {

        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        user.setId(1);
        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);


        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllByUserOrderByCreatedAtDesc(user,new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getMyUserFriendPosts(user,new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }


    @Test
    public void generatePostFromDTO_Should_Return_A_Post() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        user.setId(1);
        PostDTO postDTO=new PostDTO("postDTOText",true);

        // Act
        Post post=postService.generatePostFromDTO(user,postDTO);
        // Assert
        Assert.assertEquals("postDTOText",post.getPostText());
    }

    @Test(expected = EntityBadRequestException.class)
    public void generatePostFromDTO_Throws_An_Exception_When_PostTextEmpty() {
        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        user.setId(1);
        PostDTO postDTO=new PostDTO(null,true);

        // Act
        Post post=postService.generatePostFromDTO(user,postDTO);}



    @Test
    public void getMineAndSharedAndFriendsPosts_Should_Return_A_PageOfPost() {

        // Arrange
        User user=new User("pass1","fName1","lName1","email1");
        User user2=new User("pass2","fName2","lName2","email2");
        Post post1 = new Post("postText1", new User("pass1", "fName1", "lName1", "email1"), true);
        Post post2 = new Post("postText2", new User("pass1", "fName1", "lName1", "email1"), true);
        Set<User> friendsAndMe = new HashSet<>();
        friendsAndMe.add(user);
        friendsAndMe.add(user2);
        List<Post> posts = new ArrayList<Post>();
        posts.add(post1);
        posts.add(post2);
        Page<Post> pagedResponse = new PageImpl(posts);
        Mockito.when(mockPostRepository.findAllBySharedIsTrueOrUserInOrderByCreatedAtDesc(friendsAndMe,new PageRequest(0, 2))).thenReturn(pagedResponse);
        // Act
        Page<Post> page = postService.getMineAndSharedAndFriendsPosts(friendsAndMe,new PageRequest(0, 2));

        // Assert
        Assert.assertEquals(2, page.getTotalElements());
        Assert.assertEquals("postText1", page.getContent().get(0).getPostText());
    }



    @Test
    public void getSetUsers_Should_Return_A_Set() {
        // Arrange

        User user=new User("pass1","fName1","lName1","email1");
        User user2=new User("pass2","fName2","lName2","email2");
        User user3=new User("pass3","fName3","lName3","email3");
        Set<User> friends = new HashSet<>();
        friends.add(user2);
        friends.add(user3);
        User user4=new User("pass4","fName4","lName4","email4");
        User user5=new User("pass5","fName5","lName5","email5");
        Set<User> friends2 = new HashSet<>();
        friends.add(user4);
        friends.add(user5);

        user.setFriendListAsRequester(friends);
        user.setFriendListAsAccepter(friends2);

        // Act
         Set<User> friendsAndMe=postService.getSetUsers(user);
        // Assert
        Assert.assertEquals(5,friendsAndMe.size());
    }

}