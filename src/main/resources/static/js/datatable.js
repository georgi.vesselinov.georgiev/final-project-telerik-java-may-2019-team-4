"use strict";
$(document).ready( function () {
    var table = $('#usersTable').DataTable({
        "sAjaxSource": "/api/v1/users/usersAdmin",
        "sAjaxDataProp": "",
        "order": [[ 0, "asc" ]],
        "aoColumns": [
            { "mData": "id"},
            { "mData": "firstName" },
            { "mData": "lastName" },
            { "mData": "email" },
            { "mData": "enabled" }
        ]
    })
});