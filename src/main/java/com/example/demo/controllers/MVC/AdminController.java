package com.example.demo.controllers.MVC;

import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AdminController {

    private UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/administration")
    public String showPage(Model model, @RequestParam(defaultValue = "0") int page) {
        model.addAttribute("data",
                userService.getAllByPage(PageRequest.of(page, 5)));
        model.addAttribute("currentPage", page);
        return "administration";
    }

    @PostMapping("/save")
    public String save(User user) {
        userService.saveUser(user);
        return "redirect:/administration";
    }

    @GetMapping("/delete")
    public String deleteUser(int id) {
        userService.deleteUser(id);
        return "redirect:/administration";
    }

    @GetMapping("enable")
    public String enableUser( int id){
        userService.enableUser(id);
        return "redirect:/administration";
    }

    @GetMapping("/findOne")
    @ResponseBody
    public User findOne(int id) {
        return userService.getUserByIdAdmin(id);
    }
}
