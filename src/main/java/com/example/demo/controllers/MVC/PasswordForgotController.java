package com.example.demo.controllers.MVC;

import com.example.demo.models.DTO.PasswordForgotDTO;
import com.example.demo.models.Mail;
import com.example.demo.models.PasswordResetToken;
import com.example.demo.models.User;
import com.example.demo.repositories.PasswordResetTokenRepository;
import com.example.demo.services.EmailSenderService;
import com.example.demo.services.contracts.PasswordResetTokenService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.nulabinc.zxcvbn.Pattern.Date;

@Controller
@RequestMapping("/forgot-password")
public class PasswordForgotController {
    private UserService userService;
    private EmailSenderService emailService;
    private PasswordResetTokenService passwordResetTokenService;

    @Autowired
    public PasswordForgotController(UserService userService, EmailSenderService emailService, PasswordResetTokenService passwordResetTokenService) {
        this.userService = userService;
        this.emailService = emailService;
        this.passwordResetTokenService = passwordResetTokenService;
    }

    @ModelAttribute("forgotPasswordForm")
    public PasswordForgotDTO forgotPasswordDto() {
        return new PasswordForgotDTO();
    }

    @GetMapping
    public String displayForgotPasswordPage() {
        return "forgot-password";
    }

    @PostMapping
    public String processForgotPasswordForm(@ModelAttribute("forgotPasswordForm") @Valid PasswordForgotDTO form,
                                            BindingResult result,
                                            HttpServletRequest request) {

        if (result.hasErrors()) {
            return "forgot-password";
        }

        User user = userService.findByEmail(form.getEmail());
        if (user == null) {
            result.rejectValue("email", null, "We could not find an account for that e-mail address.");
            return "forgot-password";
        }

        if (!user.isEnabled()) {
            result.rejectValue("email", null, "The account is disabled. Please contact administrator.");
            return "forgot-password";
        }

        generateTokenAndSendEmail(request, user);

        return "redirect:/forgot-password?success";


    }

    private void generateTokenAndSendEmail(HttpServletRequest request, User user) {
        PasswordResetToken token = new PasswordResetToken();
        token.setPasswordResetToken(UUID.randomUUID().toString());
        token.setUser(user);
        Calendar cal = Calendar.getInstance();
        token.setCreatedDate(cal.getTime());
        token.setExpireDate(30);
        passwordResetTokenService.saveToken(token);

        Mail mail = new Mail();
        mail.setFrom("socialnetworkbirds@gmail.com");
        mail.setTo(user.getEmail());
        mail.setSubject("Password Reset Request");

        Map<String, Object> model = new HashMap<>();
        model.put("token", token);
        model.put("user", user);
        model.put("action", user);
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        model.put("resetUrl", url + "/reset-password?token=" + token.getPasswordResetToken());
        mail.setModel(model);
        emailService.sendEmailTemplate(mail);
    }
}
