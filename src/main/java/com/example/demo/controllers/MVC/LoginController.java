package com.example.demo.controllers.MVC;

import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.services.contracts.PostService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LoginController {

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;

    @GetMapping("/login")
    public String showLogin(Model model) {
        model.addAttribute("users", userService.getAllAdmin());
        return "login";
    }

    @GetMapping("access-denied")
    public String showAccessDenied() {
        return "error-401";
    }

    @GetMapping("/")
    public String getHomePage(Model model, @RequestParam(defaultValue = "0") int page) {
        model.addAttribute("users", userService.getAllEnabledByPage((PageRequest.of(page, 5))));
        model.addAttribute("posts", postService.getAllSharedByPage(PageRequest.of(page, 5)));
        model.addAttribute("popularPosts", postService.getMostPopularOnes(PageRequest.of(page, 5)));
        model.addAttribute("currentPage", page);
        return "landingpage";
    }

    @GetMapping("/usersearch")
    public String showUsers(Model model, @RequestParam (defaultValue = "") String name, @RequestParam(defaultValue = "0") int page){
        model.addAttribute("users", userService.findByName(name,PageRequest.of(page, 5)));
        model.addAttribute("currentPage", page);
        return "users";
    }


}

