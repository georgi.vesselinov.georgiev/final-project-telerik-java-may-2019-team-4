package com.example.demo.controllers.MVC;

import com.example.demo.models.ConfirmationToken;
import com.example.demo.models.User;
import com.example.demo.services.EmailSenderService;
import com.example.demo.services.contracts.ConfirmationTokenService;
import com.example.demo.services.contracts.UserService;
import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class RegisterController {

    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private EmailSenderService emailService;
    private ConfirmationTokenService confirmationTokenService;

    @Autowired
    public RegisterController(PasswordEncoder passwordEncoder, UserService userService, EmailSenderService emailService, ConfirmationTokenService confirmationTokenService) {

        this.passwordEncoder=passwordEncoder;
        this.userService = userService;
        this.emailService = emailService;
        this.confirmationTokenService = confirmationTokenService;
    }

    // Return registration form template
    @RequestMapping(value="/register", method = RequestMethod.GET)
    public ModelAndView showRegistrationPage(ModelAndView modelAndView, User user){
        modelAndView.addObject("user", user);
        modelAndView.setViewName("register");
        return modelAndView;
    }

    // Process form input data
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView processRegistrationForm(ModelAndView modelAndView,User user, BindingResult bindingResult, HttpServletRequest request) {

        // Lookup user in database by e-mail
        User userExists = userService.findByEmail(user.getEmail());


        if (userExists != null) {
            modelAndView.addObject("alreadyRegisteredMessage", "Oops!  There is already a user registered with the email provided.");
            modelAndView.setViewName("register");
            bindingResult.reject("email");
        }

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("register");
        } else { // new user so we create user and send confirmation e-mail

            generateTokenAndSendViaEmail(user, request);

            modelAndView.addObject("confirmationMessage", "A confirmation e-mail has been sent to " + user.getEmail());
            modelAndView.setViewName("register");
        }

        return modelAndView;
    }

    private void generateTokenAndSendViaEmail(User user, HttpServletRequest request) {
        userService.registerUser(user);
        ConfirmationToken confirmationToken = new ConfirmationToken(user);

        confirmationTokenService.saveToken(confirmationToken);


        String appUrl = request.getScheme() + "://" + request.getServerName();

        SimpleMailMessage registrationEmail = new SimpleMailMessage();
        registrationEmail.setTo(user.getEmail());
        registrationEmail.setSubject("Registration Confirmation");
        registrationEmail.setFrom("socialnetworkbirds@gmail.com");
        registrationEmail.setText("To confirm your account, please click here : "
                + appUrl + ":8080/confirm?token=" + confirmationToken.getConfirmationToken());

        emailService.sendEmail(registrationEmail);
    }

    // Process confirmation link
    @RequestMapping(value="/confirm", method = RequestMethod.GET)
    public ModelAndView showConfirmationPage(ModelAndView modelAndView, @RequestParam("token") String token) {

        ConfirmationToken confirmationToken = confirmationTokenService.findByConfirmationToken(token);
       User user =  confirmationToken.getUser();

        if (user==null) { // No token found in DB

            modelAndView.addObject("invalidToken", "Oops!  This is an invalid confirmation link.");
        }
        else if (user.isEnabled() && !confirmationToken.getConfirmationToken().isEmpty()) {
            modelAndView.addObject("error", "This token has been used already to activate your account.");
        }
        else { // Token found

            modelAndView.addObject("confirmationToken",confirmationToken.getConfirmationToken());
        }

        modelAndView.setViewName("confirm");
        return modelAndView;
    }

    // Process confirmation link
    @RequestMapping(value="/confirm", method = RequestMethod.POST)
    public ModelAndView processConfirmationForm(ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map<String,String> requestParams, RedirectAttributes redir) {

        modelAndView.setViewName("confirm");

        Zxcvbn passwordCheck = new Zxcvbn();

        Strength strength = passwordCheck.measure(requestParams.get("password"));

        if (strength.getScore() < 3) {
            bindingResult.reject("password");

            redir.addFlashAttribute("errorMessage", "Your password is too weak.  Choose a stronger one.");

            modelAndView.setViewName("redirect:confirm?token=" + requestParams.get("token"));
            return modelAndView;
        }

        // Find the user associated with the reset token
        User user = userService.findUserByConfirmationToken(requestParams.get("token"));

        // Set new password
        user.setPassword(passwordEncoder.encode(requestParams.get("password")));

        // Set user to enabled
        user.setEnabled(true);

        // Save user
        userService.saveUser(user);

//        modelAndView.addObject("successMessage", "Your password has been set!");
        modelAndView.setViewName("redirect:/login?setSuccess");
//        confirmationTokenService.deleteToken(confirmationTokenService.findByConfirmationToken(requestParams.get("token")));
        return modelAndView;
    }


}
