package com.example.demo.controllers.MVC;

import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.DTO.UserDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.services.contracts.CommentService;
import com.example.demo.services.contracts.PostService;
import com.example.demo.services.contracts.ReplyService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
public class UserController {
    private UserService userService;
    private PostService postService;
    private CommentService commentService;
    private ReplyService replyService;


    @Autowired
    public UserController(UserService userService, PostService postService, CommentService commentService, ReplyService replyService) {
        this.userService = userService;
        this.postService = postService;
        this.commentService = commentService;
        this.replyService = replyService;
    }


    @GetMapping("/homepage")
    public String getHomePage(@RequestParam(defaultValue = "0") int page, Principal principal, Model model) {
        User user =userService.getUserByUserName(principal.getName());
        Set<User> friends=new HashSet<>(user.getFriendListAsAccepter());
        friends.addAll(user.getFriendListAsRequester());
        List<User> restUsers=userService.getAll();
        restUsers.removeAll(friends);
        restUsers.remove(user);
        Set<User> friendsAndMe=postService.getSetUsers(user);
        List<User> limitedListRestUsers=restUsers.stream().limit(10).collect(Collectors.toList());
        model.addAttribute("user", user);
        model.addAttribute("isAdmin",user.getRoles().stream().anyMatch(r->r.getName().equals("ROLE_ADMIN")));
        model.addAttribute("friendList", friends);
        model.addAttribute("restUsers", limitedListRestUsers);
        model.addAttribute("posts", postService
                .getMineAndSharedAndFriendsPosts(friendsAndMe,new PageRequest(page,5))
        );
//        model.addAttribute("adminVisiblePosts",postService.getAllPostsPageable(new PageRequest(page,5), user));
        model.addAttribute("receivedFriendRequests",user.getReceivedFriendRequests());
        model.addAttribute("sentFriendRequests",user.getSentFriendRequests());
        model.addAttribute("postDTO",new PostDTO());
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("replyDTO",new ReplyDTO());
        return "home_page";
    }


    @GetMapping("/users/{userFriendId}")
    public String getUserFriendPage(@PathVariable int userFriendId,@RequestParam(defaultValue = "0") int page, Principal principal, Model model) {
        User user =userService.getUserByUserName(principal.getName());
        User userFriend=userService.getUserById(userFriendId);
        Set<User> userFriendFriends=new HashSet<>(userFriend.getFriendListAsAccepter());
        userFriendFriends.addAll(userFriend.getFriendListAsRequester());
        model.addAttribute("user", user);
        model.addAttribute("userFriend",userFriend);
        model.addAttribute("isAdmin",user.getRoles().stream().anyMatch(r->r.getName().equals("ROLE_ADMIN")));
        model.addAttribute("userFriendFriendList", userFriendFriends);
        model.addAttribute("posts", postService
                .getMyUserFriendPosts(userFriend,new PageRequest(page,5))
        );
        model.addAttribute("postDTO",new PostDTO());
        model.addAttribute("commentDTO", new CommentDTO());
        model.addAttribute("replyDTO",new ReplyDTO());
        model.addAttribute("sharedPostsOfNonFriend",postService.getSharedPostsOfNonFriend(userFriend,new PageRequest(page,5)));
        model.addAttribute("userDTO",new UserDTO());
        return "user_friend_page";
    }


    @PostMapping("homepage/posts/new")
    public String createNewPost(@RequestParam MultipartFile file, @Valid @ModelAttribute PostDTO postDTO, BindingResult bindingResult, Principal principal, Model model){
        User user =userService.getUserByUserName(principal.getName());
        model.addAttribute("file", file);


        if (bindingResult.hasErrors()) {
            return "redirect:/homepage";
        }

        try {
            postDTO.setPostPhoto(file.getBytes());
            postService.createPost(postService.generatePostFromDTO(user,postDTO));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/homepage";
     }


    @PostMapping("/homepage/posts/update/{postId}")
    public String updatePost(@PathVariable int postId,@Valid @ModelAttribute PostDTO postDTO, BindingResult bindingResult, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        if (bindingResult.hasErrors()) {
            return "redirect:/homepage";
        }
        postService.updatePost(postId,postDTO, user);
        return "redirect:/homepage";
    }

    @GetMapping("/homepage/posts/delete/{postId}")
    public String deletePost(@PathVariable int postId, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        postService.deletePost(postId, user);
        return "redirect:/homepage";
    }


    @PostMapping("/homepage/{postId}/comments/new")
    public String createNewComment(@PathVariable int postId,@Valid @ModelAttribute CommentDTO commentDTO, BindingResult bindingResult, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        Post post=postService.getPostById(postId);

        if (bindingResult.hasErrors()) {
            return "redirect:/homepage";
        }

        commentService.createComment(commentService.generateCommentFromDTO(post,user,commentDTO));
        return "redirect:/homepage";
    }


    @GetMapping("/homepage/comments/delete/{commentId}")
    public String deleteComment(@PathVariable int commentId, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        commentService.deleteComment(commentId, user);
        return "redirect:/homepage";
    }

    @PostMapping("/homepage/{commentId}/replies/new")
    public String createNewReply(@PathVariable int commentId,@Valid @ModelAttribute ReplyDTO replyDTO, BindingResult bindingResult, Principal principal){
        User user =userService.getUserByUserName(principal.getName());


        if (bindingResult.hasErrors()) {
            return "redirect:/homepage";
        }

        replyService.createReply(replyService.generateReplyFromDTO(commentService.getCommentById(commentId),user,replyDTO));
        return "redirect:/homepage";
    }


    @GetMapping("/homepage/replies/delete/{replyId}")
    public String deleteReply(@PathVariable int replyId, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        replyService.deleteReply(replyId, user);
        return "redirect:/homepage";
    }


    @GetMapping("/requests/send/{receiverId}")
    public String sendFriendRequest(@PathVariable int receiverId,Principal principal){
        User userSender =userService.getUserByUserName(principal.getName());
        User userReceiver =userService.getUserById(receiverId);
        userService.sendFriendRequest(userSender,userReceiver);
        return "redirect:/users/"+receiverId;
    }

    @GetMapping("/requests/accept/{senderId}")
    public String acceptFriendRequest(@PathVariable int senderId,Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        int receiverId=user.getId();
        userService.acceptFriendRequest(senderId,receiverId);
        return "redirect:/homepage";
    }

    @GetMapping("/requests/reject/{otherUserId}")
    public String rejectFriendRequest(@PathVariable int otherUserId,Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        int myId=user.getId();
        userService.rejectFriendRequest(otherUserId,myId);
        return "redirect:/homepage";
    }


    @GetMapping("/friendship/remove/{otherUserId}")
    public String removeFriendship(@PathVariable int otherUserId,Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        int myId=user.getId();
        userService.deleteFriendship(myId,otherUserId);
        return "redirect:/homepage";

    }



    @GetMapping("/user/privacy/public/{userId}")
    public String setUserPublic(@PathVariable int userId){
        userService.setPublic(userId);
        return "redirect:/users/"+userId;
    }

    @GetMapping("/user/privacy/private/{userId}")
    public String setUserPrivate(@PathVariable int userId){
        userService.setPrivate(userId);
        return "redirect:/users/"+userId;
    }

    @PostMapping("/user/update")
    public String updateUserProfile(@RequestParam MultipartFile file, @Valid @ModelAttribute UserDTO userDTO, BindingResult bindingResult, Principal principal,Model model){
        User user =userService.getUserByUserName(principal.getName());
        int userId=user.getId();
        model.addAttribute("file", file);

        if (bindingResult.hasErrors()) {
            return "redirect:/users/"+userId;
        }
        try {
            userDTO.setPhoto(file.getBytes());
            userService.updateUserProfile(user,userDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/users/"+userId;
    }



}