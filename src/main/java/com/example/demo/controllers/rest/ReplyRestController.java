package com.example.demo.controllers.rest;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityForbiddenRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Reply;
import com.example.demo.models.User;
import com.example.demo.services.contracts.CommentService;
import com.example.demo.services.contracts.ReplyService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/replies")
public class ReplyRestController {
    private ReplyService replyService;
    private UserService userService;
    private CommentService commentService;


    @Autowired
    public ReplyRestController(ReplyService replyService, UserService userService, CommentService commentService) {
        this.replyService = replyService;
        this.userService = userService;
        this.commentService = commentService;
    }

    @GetMapping("/all")
    public List<Reply> getAllReplies(){
        return replyService.getAllReplies();
    }

    @GetMapping("/{id}")
    public Reply getReplyById(@PathVariable int id){
        try {
            return replyService.getReplyById(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteReplyById(@PathVariable int id,Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            replyService.deleteReply(id,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }

    }

//    @PostMapping
//    public void createReply(@RequestBody Reply reply){
//        try {
//            replyService.createReply(reply);
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }


//    @PutMapping("/{id}")
//    public void updateReply(@PathVariable int id,@RequestBody ReplyDTO replyDTO){
//        try {
//            replyService.updateReply(id,replyDTO);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }


    @PutMapping("/{replyId}/new_likes")
    public void addReplyLike(@PathVariable int replyId,
                               Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            replyService.addReplyLike(replyId,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/{replyId}/likes")
    public void removeReplyLike(@PathVariable int replyId,
                                  Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        int userId =user.getId();
        try {
            replyService.removeReplyLike(replyId,userId);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


//    @PutMapping("/{replyId}/likes")
//    public int addRemoveAndCountReplyLike(@PathVariable int replyId,
//                                            Principal principal){
//        User user =userService.getUserByUserName(principal.getName());
//        return replyService.addRemoveAndCountReplyLike(replyId,user);
//    }

    @PutMapping("/{replyId}/likes")
    public String addRemoveAndCountReplyLike(@PathVariable int replyId,
                                          Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        return replyService.addRemoveAndCountReplyLike(replyId,user);
    }

    @PostMapping("/{commentId}/replies")
    public void createReply(@PathVariable int commentId,
                            @RequestBody ReplyDTO replyDTO,
                            Principal principal){
        Comment comment=commentService.getCommentById(commentId);
        User user=userService.getUserByUserName(principal.getName());
        try {
            replyService.createReply(replyService.generateReplyFromDTO(comment, user, replyDTO));
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityBadRequestException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
