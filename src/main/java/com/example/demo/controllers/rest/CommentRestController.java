package com.example.demo.controllers.rest;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityForbiddenRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.services.contracts.CommentService;
import com.example.demo.services.contracts.PostService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RequestMapping("/api/v1/comments")
@RestController
public class CommentRestController {
    private CommentService commentService;
    private UserService userService;
    private PostService postService;


    @Autowired
    public CommentRestController(CommentService commentService, UserService userService, PostService postService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping("/all")
    public List<Comment> getAllComments(){
        return   commentService.getAllComments();
    }

    @GetMapping("/{id}")
    public Comment getCommentById(@PathVariable int id){
        try {
            return commentService.getCommentById(id);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable int id, Principal principal){
        User user =userService.getUserByUserName(principal.getName());
        try {
            commentService.deleteComment(id, user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityForbiddenRequestException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }


//    @PutMapping("/{id}")
//    public void updateComment(@PathVariable int id,@RequestBody CommentDTO commentDTO){
//        try {
//            commentService.updateComment(id,commentDTO);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }

    @PutMapping("/{commentId}/new_like")
    public void addCommentLike(@PathVariable int commentId,Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            commentService.addCommentLike(commentId,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityAlreadyExistsException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{commentId}/likes")
    public void removeCommentLike(@PathVariable int commentId, Principal principal){
        User user=userService.getUserByUserName(principal.getName());
        try {
            commentService.removeCommentLike(commentId,user);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


//    @PostMapping("/comment")
//    public void createComment(@RequestBody Comment comment){
//        try {
//            commentService.createComment(comment);
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//
//        }
//    }
//



    @PostMapping("/post/{postId}")
    public void createComment(@PathVariable int postId,
                              @RequestBody CommentDTO commentDTO,
                              Principal principal){
        Post post=postService.getPostById(postId);
        User user=userService.getUserByUserName(principal.getName());
        try {
            commentService.createComment(commentService.generateCommentFromDTO(post,user,commentDTO));
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (EntityBadRequestException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

//    @PostMapping("/{commentId}/replies/{userId}")
//    public void createReply(@PathVariable int commentId,
//                            @PathVariable int userId,
//                            @RequestBody ReplyDTO replyDTO){
//        Comment comment=commentService.getCommentById(commentId);
//        User user=userService.getUserById(userId);
//        try {
//            commentService.createReply(comment, user, replyDTO);
//        }catch (EntityNotFoundException e){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }catch (EntityBadRequestException e){
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }





    @PutMapping("/{commentId}/likes")
    public String addRemoveAndCountCommentLike(@PathVariable int commentId,
                                         Principal principal) {
        User user = userService.getUserByUserName(principal.getName());
        return commentService.addRemoveAndCountCommentLike(commentId,user);
    }


}
