package com.example.demo.controllers.rest;

import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.User;
import com.example.demo.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Social Network application")
@RestController
@RequestMapping(path = "api/v1/admin")
public class UserAdminRestController {

    private UserService userService;

    @Autowired
    public UserAdminRestController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "Get a list with available users - both enabled and disabled", response = List.class)
    @GetMapping
    public List<User> getAllAdmin() {
        return userService.getAllAdmin();
    }


    @ApiOperation(value = "Delete user", response = void.class)
    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable(name = "id") @Valid int userId) {
        try {
            userService.deleteUser(userId);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    @ApiOperation(value = "Enable user", response = void.class)
    @PutMapping(path = "{id}")
    public void enable(@PathVariable(name = "id") @Valid int userId, @RequestBody User user) {
        try {
            userService.enableUser(userId);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
}
