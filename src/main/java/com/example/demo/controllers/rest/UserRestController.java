package com.example.demo.controllers.rest;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.DTO.UserDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Api(value = "Social Network application")
@RestController
@RequestMapping(path = "api/v1/users")
public class UserRestController {

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation(value = "Get a list with available users", response = List.class)
    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @ApiOperation(value = "Get user by id", response = User.class)
    @GetMapping(path = "{id}")
    public User getById(@PathVariable(name = "id") @Valid int userId) {
        return userService.getUserById(userId);
    }

    @ApiOperation(value = "Create user", response = void.class)
    @PostMapping
    public void create(@RequestBody @Valid User user) {
        try {
            userService.registerUser(user);
        } catch (EntityAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.MULTIPLE_CHOICES, e.getMessage());
        }
    }


    @ApiOperation(value = "Update user", response = void.class)
    @PutMapping(path = "update")
    public void update(@RequestBody @Valid UserDTO userDTO, Principal principal) {
        User user=userService.getUserByUserName(principal.getName());
        userService.updateUserProfile(user, userDTO);
    }

    @ApiOperation(value = "Show user friends", response = List.class)
    @GetMapping(path = "/all_friends")
    public Set<User> showAllUserFriends(Principal principal) {
        User user=userService.getUserByUserName(principal.getName());
        Set<User> friends=new HashSet<>(user.getFriendListAsAccepter());
        friends.addAll(user.getFriendListAsRequester());
        return friends;
    }


    @ApiOperation(value = "Send friend request", response = void.class)
    @PutMapping(path = "/request/{receiverId}")
    public void sendFriendRequest(@PathVariable int receiverId, Principal principal) {
        User userSender=userService.getUserByUserName(principal.getName());
        User userReceiver = userService.getUserById(receiverId);

        try {
            userService.sendFriendRequest(userSender, userReceiver);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (EntityBadRequestException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityAlreadyExistsException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Reject friend request", response = void.class)
    @PutMapping(path = "{senderId}/reject")
    public void rejectFriendRequest(@PathVariable int senderId,
                                    Principal principal) {
        User userReceiver=userService.getUserByUserName(principal.getName());
        int receiverId =userReceiver.getId();
        try {
            userService.rejectFriendRequest(senderId, receiverId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Cancel friend request", response = void.class)
    @PutMapping(path = "{receiverId}/cancel")
    public void cancelFriendRequest(@PathVariable int receiverId,
                                    Principal principal) {
        User userSender=userService.getUserByUserName(principal.getName());
        int senderId =userSender.getId();
        try {
            userService.rejectFriendRequest(senderId, receiverId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Accept friend request", response = void.class)
    @PutMapping(path = "{senderId}/accept")
    public void acceptFriendRequest(@PathVariable int senderId,
                                    Principal principal) {
        User userReceiver=userService.getUserByUserName(principal.getName());
        int receiverId =userReceiver.getId();
        try {
            userService.acceptFriendRequest(senderId, receiverId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Delete friend from friend list", response = void.class)
    @PutMapping(path = "/delete/{removedUserId}")
    public void deleteFriendship(@PathVariable int removedUserId,
                                 Principal principal) {
        User user=userService.getUserByUserName(principal.getName());
        int userId =user.getId();

        try {
            userService.deleteFriendship(userId, removedUserId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}

