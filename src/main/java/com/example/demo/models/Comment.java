package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "comments")
@Proxy(lazy = false)
public class Comment extends AuditModel implements Comparable<Comment>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int commentId;

    @NotNull
    @Column(name = "comment_text")
    private String commentText;

//    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @JsonIgnore
    @ManyToOne
    @NotNull
    @JoinColumn(name = "user_id")
    private User user;

//    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "comment_likes",
            joinColumns = @JoinColumn(name = "comment_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> commentLikes;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "comment_id")
    private Set<Reply> replies;



    public Comment() {
        super();
    }

    public Comment(Post post, User user, String commentText) {
        this.commentText = commentText;
        this.post=post;
        this.user=user;
        commentLikes=new HashSet<>();
        replies=new HashSet<>();
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Set<User> getCommentLikes() {
        return commentLikes;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCommentLikes(Set<User> commentLikes) {
        this.commentLikes = commentLikes;
    }

    public Set<Reply> getReplies() {
        return replies;
    }

    public void setReplies(Set<Reply> replies) {
        this.replies = replies;
    }

    @Override
    public int compareTo(Comment o) {
        return this.getCreatedAt().compareTo(o.getCreatedAt());
    }

    @JsonIgnore
    public List<Reply> getOrderedList(){
        return getReplies()
                .stream()
                .sorted(Comparator.comparing(Reply::getCreatedAt))
                .collect(Collectors.toList());
    }

}
