package com.example.demo.models.DTO;

import javax.validation.constraints.NotEmpty;
import java.util.Base64;

public class UserDTO {

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    private String nationality;
    private String city;
    private byte[] photo;

    public UserDTO() {
    }

    public UserDTO(String firstName, String lastName, String nationality, String city, byte[] photo, boolean shared) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
        this.city = city;
        this.photo = photo;
    }

    public UserDTO(String firstName, String lastName, String nationality, String city, boolean shared) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nationality = nationality;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getStringPhoto() {
        if(photo!=null){
            return Base64.getEncoder().encodeToString(photo);
        }
        return null;
    }

}

