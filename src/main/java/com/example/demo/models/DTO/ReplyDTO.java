package com.example.demo.models.DTO;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class ReplyDTO {

    @NotEmpty
    @Size(max = 2000)
    private String replyText;

    public ReplyDTO() {
    }

    public ReplyDTO(String replyText) {
        this.replyText = replyText;
    }

    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }
}
