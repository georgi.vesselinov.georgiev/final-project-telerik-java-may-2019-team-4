package com.example.demo.models.DTO;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Base64;

public class PostDTO {

    @NotEmpty
    @Size(max = 2000)
    private String postText;
    private byte[] postPhoto;
    private boolean shared;

    public PostDTO() {
    }

    public PostDTO(String postText, byte[] postPhoto, boolean shared) {
        this.postText = postText;
        this.postPhoto = postPhoto;
        this.shared = shared;
    }


    public PostDTO(String postText,boolean shared) {
        this.postText = postText;
        this.shared = shared;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }
//
//    public String getPostPhoto() {
//        if(postPhoto!=null){
//            return Base64.getEncoder().encodeToString(postPhoto);
//        }
//        return null;
//    }


    public byte[] getPostPhoto() {
        return postPhoto;
    }

    public void setPostPhoto(byte[] postPhoto) {
        this.postPhoto = postPhoto;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

}
