package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "replies")
public class Reply extends AuditModel implements Comparable<Reply>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reply_id")
    private int replyId;

    @NotNull
    @Column(name = "reply_text")
    private String replyText;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "comment_id")
    private Comment comment;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    //@JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "reply_likes",
            joinColumns = {@JoinColumn(name = "reply_id")},
            inverseJoinColumns = {@JoinColumn(name="user_id")})
    private Set<User> replyLikes;


    public Reply() {
        super();
    }

    public Reply(String replyText,Comment comment,User user) {
        super();
        this.replyText = replyText;
        this.comment = comment;
        this.user = user;
        replyLikes=new HashSet<>();
    }

    public int getReplyId() {
        return replyId;
    }

    public void setReplyId(int replyId) {
        this.replyId = replyId;
    }

    public String getReplyText() {
        return replyText;
    }

    public void setReplyText(String replyText) {
        this.replyText = replyText;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<User> getReplyLikes() {
        return replyLikes;
    }

    public void setReplyLikes(Set<User> replyLikes) {
        this.replyLikes = replyLikes;
    }

    @Override
    public int compareTo(Reply o) {
        return this.getCreatedAt().compareTo(o.getCreatedAt());
    }
}
