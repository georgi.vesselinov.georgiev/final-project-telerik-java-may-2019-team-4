package com.example.demo.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.domain.Page;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.awt.print.Pageable;
import java.util.*;
import java.util.stream.Collectors;

@Table(name="posts")
@Entity
public class Post extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="post_id")
    private int postId;

    @JsonIgnore
    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotEmpty
    @NotNull
    @Column(name = "post_text")
    private String postText;

    @Lob
    @Column(name =  "post_photo", columnDefinition = "MEDIUMBLOB")
    private byte[] postPhoto;

    @Column(name = "shared")
    private boolean shared;

//    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "post_likes",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> postLikes;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id")
    private Set<Comment> comments;


    public Post() {
    }


    public Post(String postText,User user, boolean shared) {
        this.postText = postText;
        this.user=user;
        comments=new HashSet<>();
        postLikes=new HashSet<>();
        this.shared=shared;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String getPostPhoto() {

        return Base64.getEncoder().encodeToString(postPhoto);
    }

    public void setPostPhoto(byte[] postPhoto) {
        this.postPhoto = postPhoto;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    public Set<Comment> getComments() {
        return comments;
    }
//    public Page<Comment> getComments(Pageable pageable) {
//        return comments;
//    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<User> getPostLikes() {
        return postLikes;
    }


    @JsonIgnore

    public List<Comment> getOrderedList(){
        return getComments()
                .stream()
                .sorted(Comparator.comparing(Comment::getCreatedAt))
                .collect(Collectors.toList());
    }

    public int getRepliesCount(){
        return this.getComments()
                .stream()
                .map(Comment::getReplies)
                .mapToInt(Set::size)
                .sum();
    }

    @JsonIgnore
    public List<Comment> getOrderedListLimited(){
        return getComments()
                .stream()
                .sorted(Comparator.comparing(Comment::getCreatedAt)).limit(5)
                .collect(Collectors.toList());
    }

}
