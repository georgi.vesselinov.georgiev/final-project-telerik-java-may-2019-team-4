package com.example.demo.services;

import com.example.demo.exceptions.*;
import com.example.demo.models.ConfirmationToken;
//import com.example.demo.models.Role;
import com.example.demo.models.DTO.UserDTO;
import com.example.demo.models.Role;
import com.example.demo.models.User;
import com.example.demo.repositories.ConfirmationTokenRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final String USER_NOT_FOUND = "User not found";
    private static final String USER_ALREADY_EXISTS = "User already exists.";
    private static final String PASSWORDS_DO_NOT_MATCH = "Passwords do not match.";
    private static final String EMAIL_ADDRESS_ALREADY_EXISTS = "Email address already exists.";
    private static final String USER_CANNOT_REQUEST_HIMSELF = "Sender and receiver of request cannot be the same person.";
    private static final String REQUEST_ALREADY_EXISTS = "User has already send this request and it is pending.";
    private static final String REVERSE_REQUEST_ALREADY_EXISTS = "User has already received request from this person and it is pending.";
    private static final String FRIEND_REQUEST_NOT_EXISTS = "User with id %d has not sent friend request to user with id %d.";
    private static final String FRIENDSHIP_ALREADY_EXISTS = "User with id %d is already friend with user with id %d.";
    private static final String FRIENDSHIP_NOT_EXISTS = "User with id %d is not a friend with user with id %d.";
    private static final String THE_ACCOUNT_IS_ALREADY_DISABLED = "The account is already disabled";
    private static final String USER_PRIVACY_AlREADY_CHANGED = "User privacy already changed to desired value.";

    private UserRepository userRepository;
    private ConfirmationTokenRepository confirmationTokenRepository;



    @Autowired
    public UserServiceImpl(UserRepository userRepository, ConfirmationTokenRepository confirmationTokenRepository) {
        this.userRepository = userRepository;

        this.confirmationTokenRepository = confirmationTokenRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAllByEnabledTrue();
    }

    public List<User> getAllAdmin() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(int userId) {
        if (userRepository.findByIdAndEnabledTrue(userId).isPresent()) {
            return userRepository.findByIdAndEnabledTrue(userId).get();
        } else {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }
    }

    public User getUserByIdAdmin(int userid) {
        if (userRepository.findById(userid).isPresent()) {
            return userRepository.findById(userid).get();
        } else {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }
    }

    public User getUserByUserName(String username) {
        if (userRepository.findByEmailAndEnabledFalse(username).isPresent()){
            throw new EntityAlreadyDisabledException(THE_ACCOUNT_IS_ALREADY_DISABLED);
        }
        if (userRepository.findByEmailAndEnabledTrue(username).isPresent()) {
            return userRepository.findByEmailAndEnabledTrue(username).get();
        } else {
            throw new EntityNotFoundException(USER_NOT_FOUND);
        }
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public User findUserByConfirmationToken(String confirmationToken) {
        return confirmationTokenRepository.findByConfirmationToken(confirmationToken).getUser();

    }

    public User registerUser(User user) {
        List<User> users = userRepository.findAll().stream()
                .filter(u->u.getEmail().equals(user.getEmail()))
                .collect(Collectors.toList());

        if (users.size()>0){
            throw new EntityAlreadyExistsException(USER_ALREADY_EXISTS);
        }
        user.setFirstName(user.getFirstName());
        user.setLastName(user.getLastName());
        user.setEmail(user.getEmail());
        user.setRoles(Arrays.asList(new Role("ROLE_USER")));
        return userRepository.save(user);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

//    @Override
//    @Transactional
//    public void updateUser(int userId, User user) {
//        User userToUpdate = getUserById(userId);
//        if (user.getEmail() != null && !userHasEmail(user.getEmail())) {
//            userToUpdate.setEmail(user.getEmail());
//        } else {
//            throw new EntityEmailAddressExistsException(EMAIL_ADDRESS_ALREADY_EXISTS);
//        }
//        if (user.getPhoto() != null) {
//            userToUpdate.setPhoto(user.getPhoto().getBytes());
//        } else {
//            byte[] picture = userToUpdate.getPhoto().getBytes();
//            userToUpdate.setPhoto(picture);
//        }
//        userRepository.save(userToUpdate);
//    }

    public void updatePassword(String password, int id) {

        userRepository.updatePassword(password, id);
    }

    @Override
    @Transactional
    public void deleteUser(int userId) {
        User userToDelete = getUserByIdAdmin(userId);
        if (!userToDelete.isEnabled()){
            throw new EntityAlreadyDisabledException(THE_ACCOUNT_IS_ALREADY_DISABLED);
        }
        userToDelete.setEnabled(false);
        userRepository.save(userToDelete);
    }

    public void enableUser(int userId) {
        User userToEnable = getUserByIdAdmin(userId);
        userToEnable.setEnabled(true);
        userRepository.save(userToEnable);
    }

    @Override
    public boolean userHasEmail(String email) {
        Optional<User> userByEmail = userRepository.findByEmailAndEnabledTrue(email);
        return userByEmail.isPresent();
    }

    @Override
    public void sendFriendRequest(User sender, User receiver) {
        int senderId=sender.getId();
        int receiverId=receiver.getId();
        if (senderId == receiverId) {
            throw new EntityBadRequestException(USER_CANNOT_REQUEST_HIMSELF);
        }
        if (sender.getSentFriendRequests().stream().anyMatch(u -> u.getId() == receiverId)) {
            throw new EntityAlreadyExistsException(REQUEST_ALREADY_EXISTS);
        }
        if (sender.getReceivedFriendRequests().stream().anyMatch(u -> u.getId() == receiverId)) {
            throw new EntityAlreadyExistsException(REVERSE_REQUEST_ALREADY_EXISTS);
        }
        if (sender.getFriendListAsRequester().stream().anyMatch(u -> u.getId() == receiverId) ||
                receiver.getFriendListAsRequester().stream().anyMatch(u -> u.getId() == senderId)) {
            throw new EntityAlreadyExistsException(String.format(FRIENDSHIP_ALREADY_EXISTS, senderId, receiverId));
        }
        sender.getSentFriendRequests().add(receiver);
        receiver.getReceivedFriendRequests().add(sender);
        userRepository.save(sender);
        userRepository.save(receiver);
    }

    @Override
    public void rejectFriendRequest(int senderId, int receiverId) {
        User sender = getUserById(senderId);
        User receiver = getUserById(receiverId);
//        checkAndRemoveRequestIfExists(senderId, receiverId, sender, receiver);
        if (sender.getSentFriendRequests().stream().noneMatch(u -> u.getId() == receiverId) &&
                sender.getReceivedFriendRequests().stream().noneMatch(u -> u.getId() == receiverId)) {
            throw new EntityNotFoundException(String.format(FRIEND_REQUEST_NOT_EXISTS, senderId, receiverId));
        }
        sender.getSentFriendRequests().removeIf(u -> u.getId() == receiverId);
        sender.getReceivedFriendRequests().removeIf(u -> u.getId() == receiverId);
        receiver.getReceivedFriendRequests().removeIf(u -> u.getId() == senderId);
        receiver.getSentFriendRequests().removeIf(u -> u.getId() == senderId);



        userRepository.save(sender);
        userRepository.save(receiver);
    }


    @Override
    public void acceptFriendRequest(int senderId, int receiverId) {
        User sender = getUserById(senderId);
        User receiver = getUserById(receiverId);
        checkAndRemoveRequestIfExists(senderId, receiverId, sender, receiver);
        sender.getFriendListAsRequester().add(receiver);
        receiver.getFriendListAsAccepter().add(sender);
        userRepository.save(sender);
        userRepository.save(receiver);
    }

    @Override
    public void deleteFriendship(int senderId, int receiverId) {
        User sender = getUserById(senderId);
        User receiver = getUserById(receiverId);
        if (sender.getFriendListAsRequester().stream().noneMatch(u -> u.getId() == receiverId) &&
                sender.getFriendListAsAccepter().stream().noneMatch(u -> u.getId() == receiverId)) {
            throw new EntityNotFoundException(String.format(FRIENDSHIP_NOT_EXISTS, senderId, receiverId));
        }
        sender.getFriendListAsRequester().removeIf(u -> u.getId() == receiverId);
        receiver.getFriendListAsAccepter().removeIf(u -> u.getId() == senderId);
        sender.getFriendListAsAccepter().removeIf(u -> u.getId() == receiverId);
        receiver.getFriendListAsRequester().removeIf(u -> u.getId() == senderId);
        userRepository.save(sender);
        userRepository.save(receiver);
    }

    private void checkAndRemoveRequestIfExists(int senderId, int receiverId, User sender, User receiver) {
        if (sender.getSentFriendRequests().stream().noneMatch(u -> u.getId() == receiverId)) {
            throw new EntityNotFoundException(String.format(FRIEND_REQUEST_NOT_EXISTS, senderId, receiverId));
        }
        sender.getSentFriendRequests().removeIf(u -> u.getId() == receiverId);
        receiver.getReceivedFriendRequests().removeIf(u -> u.getId() == senderId);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Birdy not found");
        }

        if (!user.isEnabled()){
            throw new InternalAuthenticationServiceException("Account has been disabled. Please contact administrator.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    public Page<User> getAllByPage(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    public Page<User> getAllEnabledByPage(Pageable pageable) {
        return userRepository.findAllByEnabledTrueAndSharedTrue(pageable);
    }


    @Override
    public void updateUserProfile(User user, UserDTO userDTO) {
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setNationality(userDTO.getNationality());
        user.setCity(userDTO.getCity());
        byte[] picture=user.getPhotoBytes();
        if(userDTO.getStringPhoto().isEmpty()){
            user.setPhoto(picture);
        }else {
            user.setPhoto(userDTO.getPhoto());
        }
        userRepository.save(user);
    }



    @Override
    public void setPrivate(int id) {
        User user= getUserById(id);
        if (!user.isShared()){
            throw new EntityAlreadyChangedException(USER_PRIVACY_AlREADY_CHANGED);
        }
        user.setShared(false);
        userRepository.save(user);
    }

    @Override
    public void setPublic(int id) {
        User user= getUserById(id);
        if (user.isShared()){
            throw new EntityAlreadyChangedException(USER_PRIVACY_AlREADY_CHANGED);
        }
        user.setShared(true);
        userRepository.save(user);
    }

    public Page<User> findByName(String firstName, Pageable pageable){
        return userRepository.findAllByEnabledTrueAndFirstNameLike("%" + firstName + "%",pageable);
    }
}
