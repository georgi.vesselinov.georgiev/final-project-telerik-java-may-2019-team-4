package com.example.demo.services;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityForbiddenRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Post;
import com.example.demo.models.Reply;
import com.example.demo.models.User;
import com.example.demo.repositories.CommentRepository;
import com.example.demo.services.contracts.CommentService;
import com.example.demo.services.contracts.ReplyService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentServiceImpl implements CommentService {
    private static final String COMMENT_NOT_FOUND = "Comment not found.";
    private static final String COMMENT_CAN_NOT_BE_EMPTY = "Comment cannot have no text.";
    private static final String LIKE_ALREADY_EXISTS = "Comment has already been liked by the user.";
    private static final String LIKE_NOT_EXISTS = "Comment has not been liked by the user.";
    private static final String REPLY_CAN_NOT_BE_EMPTY = "Reply should contain text.";
    private static final String NOT_AUTHORIZED = "You have not created the comment, nor you are admin.";

    private CommentRepository commentRepository;
    private ReplyService replyService;


    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository, ReplyService replyService) {
        this.commentRepository = commentRepository;
        this.replyService = replyService;

    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public Comment getCommentById(int id) {
//        Optional<Comment> comment=commentRepository.findById(id);
//        return comment.orElseThrow(()-> new EntityNotFoundException(COMMENT_NOT_FOUND));
        Optional<Comment> comment = commentRepository.findById(id);
        if (!comment.isPresent()) {
            throw new EntityNotFoundException(COMMENT_NOT_FOUND);
        }
        return comment.get();
    }

    @Override
    public void deleteComment(int id, User user) {
        if (getCommentById(id).getUser().getId()!=user.getId()&&user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        deleteAllRepliesOfComment(id);
        commentRepository.deleteById(id);
    }


    public void deleteAllRepliesOfComment(int commentId){
        Comment comment = getCommentById(commentId);
        for (Reply reply : comment.getReplies()) {
            replyService.deleteReply(reply.getReplyId(), reply.getUser()); }
    }


//    @Override
//    public void deleteComment(int id) {
//        Comment comment = getCommentById(id);
//        for (Reply reply : comment.getReplies()) {
//            replyService.deleteReply(reply.getReplyId());
//        }
//        commentRepository.deleteById(id);
//    }

    @Override
    public void createComment(Comment comment) {
        if (comment.getCommentText().isEmpty()) {
            throw new EntityBadRequestException(COMMENT_CAN_NOT_BE_EMPTY);
        }
        commentRepository.save(comment);
    }

    @Override
    public void updateComment(int id, CommentDTO commentDTO) {
        Comment commentToBeUpdated = getCommentById(id);
        if (commentDTO.getCommentText() == null) {
            throw new EntityBadRequestException(COMMENT_CAN_NOT_BE_EMPTY);
        }
        String commentText = commentDTO.getCommentText();
        commentToBeUpdated.setCommentText(commentText);
        commentRepository.save(commentToBeUpdated);
    }

    @Override
    public int addCommentLike(int commentId, User user) {
        Comment comment = getCommentById(commentId);
        int userId=user.getId();
        if (comment.getCommentLikes().stream().anyMatch(u -> u.getId() == userId)) {
            throw new EntityAlreadyExistsException(LIKE_ALREADY_EXISTS);
        }
        comment.getCommentLikes().add(user);
        commentRepository.save(comment);
        return comment.getCommentLikes().size();
    }

    @Override
    public int removeCommentLike(int commentId, User user) {
        Comment comment = getCommentById(commentId);
        int userId=user.getId();
        if (comment.getCommentLikes().stream().noneMatch(u -> u.getId() == userId)) {
            throw new EntityNotFoundException(LIKE_NOT_EXISTS);
        }
        comment.getCommentLikes().removeIf(u -> u.getId() == userId);  //!!!!! ремуув от сет !!!
        commentRepository.save(comment);
        return comment.getCommentLikes().size();
    }


//    @Override
//    public void createReply(Comment comment, User user, ReplyDTO replyDTO) {
//
//        if (replyDTO.getReplyText() == null) {
//            throw new EntityBadRequestException(REPLY_CAN_NOT_BE_EMPTY);
//        }
//        String replyText = replyDTO.getReplyText();
//        Reply reply = new Reply(replyText, comment, user);
//        replyService.createReply(reply); }


    @Override
    public Comment generateCommentFromDTO(Post post, User user, CommentDTO commentDTO) {
        if (commentDTO.getCommentText() == null) {
            throw new EntityBadRequestException(COMMENT_CAN_NOT_BE_EMPTY);
        }
        String commentText = commentDTO.getCommentText();
        return new Comment(post, user, commentText);
    }



    @Override
    public String addRemoveAndCountCommentLike(int commentId, User user) {
        Comment comment = getCommentById(commentId);
        int userId=user.getId();
        if (comment.getCommentLikes().stream().noneMatch(u -> u.getId() == userId)) {
            return addCommentLike(commentId,user)+" UnLike";
        }
            return removeCommentLike(commentId,user)+" Like";
    }

}
