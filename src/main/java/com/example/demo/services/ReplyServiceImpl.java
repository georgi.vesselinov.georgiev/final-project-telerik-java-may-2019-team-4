package com.example.demo.services;

import com.example.demo.exceptions.EntityAlreadyExistsException;
import com.example.demo.exceptions.EntityBadRequestException;
import com.example.demo.exceptions.EntityForbiddenRequestException;
import com.example.demo.exceptions.EntityNotFoundException;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.ReplyDTO;
import com.example.demo.models.Reply;
import com.example.demo.models.User;
import com.example.demo.repositories.ReplyRepository;
import com.example.demo.services.contracts.ReplyService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReplyServiceImpl implements ReplyService {
    private static final String REPLY_NOT_FOUND = "Reply not found.";
    private static final String REPLY_CAN_NOT_BE_EMPTY = "Reply should contain text.";
    private static final String LIKE_ALREADY_EXISTS = "Reply has already been liked by the user.";
    private static final String LIKE_NOT_EXISTS = "Reply has not been liked by the user.";
    private static final String NOT_AUTHORIZED = "You have not created the reply, nor you are admin.";

    private ReplyRepository replyRepository;


    @Autowired
    public ReplyServiceImpl(ReplyRepository replyRepository) {
        this.replyRepository = replyRepository;
    }

    @Override
    public List<Reply> getAllReplies() { return replyRepository.findAllByActiveUser();
    }

    //ако юзъра е дисейбълд ТОДО
    @Override
    public Reply getReplyById(int id) {
        Optional<Reply> reply = replyRepository.findById(id);
        if (!reply.isPresent()) {
            throw new EntityNotFoundException(REPLY_NOT_FOUND);
        }
        return reply.get();
    }

    @Override
    public void deleteReply(int id, User user) {
        Reply reply = getReplyById(id);
        if (reply.getUser().getId()!=user.getId()&& user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))
        ){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        replyRepository.deleteById(id);
    }

    @Override
    public void createReply(Reply reply) {
        if (reply.getReplyText().isEmpty()) { throw new EntityBadRequestException(REPLY_CAN_NOT_BE_EMPTY);
        }
        replyRepository.save(reply);
    }


    @Override
    public void updateReply(int id, ReplyDTO replyDTO) {
        Reply replyToBeUpdated = getReplyById(id);
        if (replyDTO.getReplyText() == null) {
            throw new EntityBadRequestException(REPLY_CAN_NOT_BE_EMPTY);
        }
        String replyText = replyDTO.getReplyText();
        replyToBeUpdated.setReplyText(replyText);
        replyRepository.save(replyToBeUpdated);
    }

    @Override
    public int addReplyLike(int replyId, User user) {
        Reply reply = getReplyById(replyId);
        int userId=user.getId();
        if (reply.getReplyLikes().stream().anyMatch(u -> u.getId() == userId)) {
            throw new EntityAlreadyExistsException(LIKE_ALREADY_EXISTS);
        }
        reply.getReplyLikes().add(user);
        replyRepository.save(reply);
        return reply.getReplyLikes().size();
    }


    @Override
    public int removeReplyLike(int replyId, int userId) {
        Reply reply = getReplyById(replyId);
        if (reply.getReplyLikes().stream().noneMatch(u -> u.getId() == userId)) {
            throw new EntityNotFoundException(LIKE_NOT_EXISTS);
        }
        reply.getReplyLikes().removeIf(u -> u.getId() == userId);  //!!!!! ремуув от сет !!!
        replyRepository.save(reply);
        return reply.getReplyLikes().size();

    }

//    @Override
//    public int addRemoveAndCountReplyLike(int replyId, User user) {
//        Reply reply = getReplyById(replyId);
//        int userId=user.getId();
//        if (reply.getReplyLikes().stream().noneMatch(u -> u.getId() == userId)) {
//            return addReplyLike(replyId,user);
//        }
//        return removeReplyLike(replyId,userId);
//    }



    @Override
    public String addRemoveAndCountReplyLike(int replyId, User user) {
        Reply reply = getReplyById(replyId);
        int userId=user.getId();
        if (reply.getReplyLikes().stream().noneMatch(u -> u.getId() == userId)) {
            return addReplyLike(replyId,user)+" UnLike";
        }
        return removeReplyLike(replyId,userId)+" Like";
    }

//    @Override
//    public void createReply(Comment comment, User user, ReplyDTO replyDTO) {
//
//        if (replyDTO.getReplyText() == null) {
//            throw new EntityBadRequestException(REPLY_CAN_NOT_BE_EMPTY);
//        }
//        String replyText = replyDTO.getReplyText();
//        Reply reply = new Reply(replyText, comment, user);
//        createReply(reply);
//        }
//    trydno se testva




    @Override
    public Reply generateReplyFromDTO(Comment comment, User user, ReplyDTO replyDTO) {

        if (replyDTO.getReplyText() == null) {
            throw new EntityBadRequestException(REPLY_CAN_NOT_BE_EMPTY);
        }
        return new Reply(replyDTO.getReplyText(), comment, user);
    }



}