package com.example.demo.services.contracts;

import com.example.demo.models.ConfirmationToken;
import org.springframework.stereotype.Service;


public interface ConfirmationTokenService {
    ConfirmationToken saveToken(ConfirmationToken confirmationToken);

    ConfirmationToken findByConfirmationToken(String confirmationToken);

    void deleteToken(ConfirmationToken confirmationToken);
}
