package com.example.demo.services.contracts;

import com.example.demo.models.PasswordResetToken;

public interface PasswordResetTokenService {
    PasswordResetToken findByPasswordResetToken(String passwordResetToken);

    PasswordResetToken saveToken(PasswordResetToken passwordResetToken);

    void deleteToken(PasswordResetToken passwordResetToken);
}
