package com.example.demo.services.contracts;


import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface PostService {

    public Post getPostById(int id);

    public void createPost(Post post);

    public void updatePost(int id, PostDTO postDTO, User user);

//    public String updatePost(int id, PostDTO postDTO);

    public void chancePrivacy(int id, PostDTO postDTO, User user);

    //public void createComment(int postId, int userId, String commentText);
//    public void createComment(int postId, int userId, CommentDTO commentDTO);

//    public void createPost(int userId, PostDTO postDTO);

    public int addPostLike(int postID, User user);

    public int removePostLike(int postId, User user);

    public void deletePost(int id, User user);

    public Page<Post> getAllPostsPageable(Pageable pageable, User user);

    public String addRemoveAndCountPostLike(int postId, User user);

//    public Page<Post> getMineAndSharedAndFriendsPosts(int userId, Pageable pageable);

    public Page<Post> getMineAndSharedAndFriendsPosts(Set<User> friendsAndMe, Pageable pageable);

     Set<User> getSetUsers(User user);

    public Page<Post> getMyUserFriendPosts(User user, Pageable pageable);

    public void setPrivate(int id);

    public void setPublic(int id);

    Page<Post> getSharedPostsOfNonFriend(User userNonFriend,Pageable pageable);

    Page<Post> getAllSharedByPage(Pageable pageable);

    Page<Post> getMostPopularOnes(Pageable pageable);

    public String changePostPrivacy(int id, User user);

    public Post generatePostFromDTO(User user, PostDTO postDTO);
}

