package com.example.demo.services.contracts;

import com.example.demo.models.DTO.UserDTO;
import com.example.demo.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {
    List<User> getAll();

    User getUserById(int userId);

    User findByEmail(String email);

    User registerUser(User user);

    List<User> getAllAdmin();

    void updatePassword(String password, int id);

    public User getUserByUserName(String username);

    User getUserByIdAdmin(int userid);

    void enableUser(int userId);

//    User getUserByUserNameDisabled(String username);

//    @Transactional
//    void addUser(User user);

    Page<User> getAllByPage(Pageable pageable);

    @Transactional
    User saveUser(User user);

    User findUserByConfirmationToken(String confirmationToken);

//    @Transactional
//    void updateUser(int userId, User user);

    //    TODO delete from other repositories as well
    @Transactional
    void deleteUser(int userId);

    boolean userHasEmail(String email);

    public void sendFriendRequest(User sender, User receiver);

        void rejectFriendRequest(int senderId, int receiverId);

    void acceptFriendRequest(int senderId, int receiverId);

    void deleteFriendship(int senderId, int receiverId);
    //void cancelFriendRequest(int senderId, int receiverId);

    void updateUserProfile(User user, UserDTO userDTO);


    Page<User> getAllEnabledByPage(Pageable pageable);

    Page<User> findByName(String firstName, Pageable pageable);

    void setPrivate(int id);

    void setPublic(int id);
}
