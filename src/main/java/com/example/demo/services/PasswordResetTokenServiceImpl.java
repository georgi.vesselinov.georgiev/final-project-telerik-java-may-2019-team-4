package com.example.demo.services;

import com.example.demo.models.ConfirmationToken;
import com.example.demo.models.PasswordResetToken;
import com.example.demo.repositories.PasswordResetTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetTokenServiceImpl implements com.example.demo.services.contracts.PasswordResetTokenService {

    private PasswordResetTokenRepository passwordResetTokenRepository;

    @Autowired
    public PasswordResetTokenServiceImpl(PasswordResetTokenRepository passwordResetTokenRepository) {
        this.passwordResetTokenRepository = passwordResetTokenRepository;
    }

    @Override
    public PasswordResetToken findByPasswordResetToken(String passwordResetToken) {
        return passwordResetTokenRepository.findByPasswordResetToken(passwordResetToken);
    }

    @Override
    public PasswordResetToken saveToken(PasswordResetToken passwordResetToken) {
        return passwordResetTokenRepository.save(passwordResetToken);
    }

    @Override
    public void deleteToken(PasswordResetToken passwordResetToken) {
        passwordResetTokenRepository.delete(passwordResetToken);
    }
}
