package com.example.demo.services;

import com.example.demo.exceptions.*;
import com.example.demo.models.Comment;
import com.example.demo.models.DTO.CommentDTO;
import com.example.demo.models.DTO.PostDTO;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.repositories.PostRepository;
import com.example.demo.services.contracts.CommentService;
import com.example.demo.services.contracts.PostService;
import com.example.demo.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {
    private static final String POST_NOT_FOUND = "Post not found.";
    private static final String POST_CAN_NOT_BE_EMPTY = "Post cannot have no text.";
    private static final String POST_PRIVACY_AlREADY_CHANGED = "Post privacy already changed to desired value.";
    private static final String LIKE_ALREADY_EXISTS = "Post has already been liked by the user.";
    private static final String LIKE_NOT_EXISTS = "Post has not been liked by the user.";
    private static final String COMMENT_CAN_NOT_BE_EMPTY = "Comment cannot have no text.";
    private static final String NOT_AUTHORIZED = "You have not created the post, nor you are admin.";


    private PostRepository postRepository;
    private CommentService commentService;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, CommentService commentService) {
        this.postRepository = postRepository;
        this.commentService = commentService;
    }


    @Override
    public Page<Post> getAllPostsPageable(Pageable pageable, User user) {
        if (user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        return postRepository.findAllByOrderByCreatedAtDesc(pageable);
    }


    @Override
    public Post getPostById(int id) {
        Optional<Post> post = postRepository.findById(id);
        if (!post.isPresent()) {
            throw new EntityNotFoundException(POST_NOT_FOUND);
        }
        return post.get();
    }

    @Override
    public void createPost(Post post) {
        if (post.getPostText().isEmpty()) {
            throw new EntityBadRequestException(POST_CAN_NOT_BE_EMPTY);
        }
        postRepository.save(post);
    }

    @Override
    public void updatePost(int id, PostDTO postDTO, User user) {
        Post postToBeUpdated = getPostById(id);
        if (postToBeUpdated.getUser().getId()!=user.getId()&&user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        if (postDTO.getPostText() == null) {
            throw new EntityBadRequestException(POST_CAN_NOT_BE_EMPTY);
        }
        String postText = postDTO.getPostText();
        postToBeUpdated.setPostText(postText);
        postRepository.save(postToBeUpdated);
    }


//    @Override
//    public String updatePost(int id, PostDTO postDTO) {
//        Post postToBeUpdated = getPostById(id);
//        if (postDTO.getPostText() == null) {
//            throw new EntityBadRequestException(POST_CAN_NOT_BE_EMPTY);
//        }
//        String postText = postDTO.getPostText();
//        postToBeUpdated.setPostText(postText);
//        postRepository.save(postToBeUpdated);
//        return postToBeUpdated.getPostText();
//    }



    @Override
    public void chancePrivacy(int id, PostDTO postDTO, User user) {
        if (getPostById(id).getUser().getId()!=user.getId()&&user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        if (getPostById(id).isShared() == postDTO.isShared()) {
            throw new EntityAlreadyChangedException(POST_PRIVACY_AlREADY_CHANGED);
        }
        getPostById(id).setShared(postDTO.isShared());
        postRepository.save(getPostById(id));
    }


    @Override
    public String changePostPrivacy(int id, User user) {
        if (getPostById(id).getUser().getId()!=user.getId()&&user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        Post postToBeUpdated = getPostById(id);
        if (postToBeUpdated.isShared()) {
            setPrivate(id);
            return " Public";
        }
        setPublic(id);
        return " Private";
    }



    @Override
    public void setPrivate(int id) {
        Post postToBeUpdated = getPostById(id);
        if (!postToBeUpdated.isShared()) {
            throw new EntityAlreadyChangedException(POST_PRIVACY_AlREADY_CHANGED);
        }
        postToBeUpdated.setShared(false);
        postRepository.save(postToBeUpdated);
    }


    @Override
    public void setPublic(int id) {
        Post postToBeUpdated = getPostById(id);
        if (postToBeUpdated.isShared()) {
            throw new EntityAlreadyChangedException(POST_PRIVACY_AlREADY_CHANGED);
        }
        postToBeUpdated.setShared(true);
        postRepository.save(postToBeUpdated);
    }

//    @Override
//    public void createComment(int postId, int userId, CommentDTO commentDTO) {
//        Post postToBeCommented = getPostById(postId);
//        User userToComment = userService.getUserById(userId);
//        if (commentDTO.getCommentText() == null) {
//            throw new EntityBadRequestException(COMMENT_CAN_NOT_BE_EMPTY);
//        }
//        String commentText = commentDTO.getCommentText();
//        Comment comment = new Comment(postToBeCommented, userToComment, commentText);
//        commentService.createComment(comment);
//    }




    @Override
    public Post generatePostFromDTO(User user, PostDTO postDTO) {
        if (postDTO.getPostText() == null) {
            throw new EntityBadRequestException(POST_CAN_NOT_BE_EMPTY);
        }

        String postText = postDTO.getPostText();
        Boolean shared = postDTO.isShared();
        Post post = new Post(postText, user, shared);
        if (postDTO.getPostPhoto() != null) {
            byte[] picture = postDTO.getPostPhoto();
            post.setPostPhoto(picture);
        }

        return post;
    }




//    @Override
//    public void createPost(int userId, PostDTO postDTO) {
//        User userToPost = userService.getUserById(userId);
//        if (postDTO.getPostText() == null) {
//            throw new EntityBadRequestException(POST_CAN_NOT_BE_EMPTY);
//        }
//
//        String postText = postDTO.getPostText();
//        Boolean shared = postDTO.isShared();
//        Post post = new Post(postText, userToPost, shared);
//        if (postDTO.getPostPhoto() != null) {
//            byte[] picture = postDTO.getPostPhoto();
//            post.setPostPhoto(picture);
//        }
//
//        createPost(post);
//    }


    @Override
    public int addPostLike(int postId, User user) {
        Post post = getPostById(postId);
        int userId=user.getId();
        if (post.getPostLikes().stream().anyMatch(u -> u.getId() == userId)) {
            throw new EntityAlreadyExistsException(LIKE_ALREADY_EXISTS);
        }
        post.getPostLikes().add(user);
        postRepository.save(post);
        return post.getPostLikes().size();
    }

    @Override
    public int removePostLike(int postId, User user) {
        Post post = getPostById(postId);
        int userId=user.getId();
        if (post.getPostLikes().stream().noneMatch(u -> u.getId() == userId)) {
            throw new EntityNotFoundException(LIKE_NOT_EXISTS);
        }
        post.getPostLikes().removeIf(u -> u.getId() == userId);  //!!!!! ремуув от сет !!!
        postRepository.save(post);
        return post.getPostLikes().size();
    }

    @Override
    public void deletePost(int id, User user) {
        if (getPostById(id).getUser().getId()!=user.getId()&&user.getRoles().stream().noneMatch(r->r.getName().equals("ROLE_ADMIN"))){
            throw new EntityForbiddenRequestException(NOT_AUTHORIZED);
        }
        Post post = getPostById(id);
        for (Comment comment : post.getComments()) {
            commentService.deleteComment(comment.getCommentId(), comment.getUser());
        }
        postRepository.deleteById(id);
    }

    @Override
    public String addRemoveAndCountPostLike(int postId, User user) {
        Post post = getPostById(postId);
        int userId=user.getId();
        if (post.getPostLikes().stream().noneMatch(u -> u.getId() == userId)) {
            return "u"+addPostLike(postId,user);
        }
        return "l"+removePostLike(postId,user);
    }

//    @Override
//    public Page<Post> getMineAndSharedAndFriendsPosts(int userId, Pageable pageable) {
//        User user = userService.getUserById(userId);
//        Set<User> friendsAndMe = new HashSet<>(user.getFriendListAsAccepter());
//        friendsAndMe.addAll(user.getFriendListAsRequester());
//        friendsAndMe.add(user);
//        return postRepository.findAllBySharedIsTrueOrUserInOrderByCreatedAtDesc(friendsAndMe, pageable);
//    }

    @Override
    public Page<Post> getMineAndSharedAndFriendsPosts(Set<User> friendsAndMe, Pageable pageable) {
        return postRepository.findAllBySharedIsTrueOrUserInOrderByCreatedAtDesc(friendsAndMe, pageable);
    }

    public Set<User> getSetUsers(User user) {
        Set<User> friendsAndMe = new HashSet<>(user.getFriendListAsAccepter());
        friendsAndMe.addAll(user.getFriendListAsRequester());
        friendsAndMe.add(user);
        return friendsAndMe;
    }


    @Override
    public Page<Post> getMyUserFriendPosts(User user, Pageable pageable) {
        return postRepository.findAllByUserOrderByCreatedAtDesc(user, pageable);
    }

    public Page<Post> getAllSharedByPage(Pageable pageable) {
        return postRepository.findAllBySharedTrueOrderByCreatedAtDesc(pageable);
    }

    public  Page<Post> getMostPopularOnes(Pageable pageable){
        return postRepository.findAllBySharedTrueOrderByCommentsDesc(pageable);
    }

    @Override
    public Page<Post> getSharedPostsOfNonFriend(User userNonFriend, Pageable pageable) {
        return postRepository.findAllByUserAndSharedIsTrueOrderByCreatedAtDesc(userNonFriend,pageable);
    }
}
