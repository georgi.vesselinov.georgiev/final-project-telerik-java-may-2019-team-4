package com.example.demo.repositories;

import com.example.demo.models.Post;
import com.example.demo.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.OrderBy;
import java.util.List;
import java.util.Set;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

//    @Query(value = "select * from posts p join users u on p.user_id=u.user_id where u.enabled=1",nativeQuery = true)
//    List<Post> findAllByActiveUser();

    Page<Post> findAllByOrderByCreatedAtDesc(Pageable pageable);// mai e nenujen

    Page<Post> findAllBySharedIsTrueOrUserInOrderByCreatedAtDesc(Set<User> friendsAndMe, Pageable pageable);

    Page<Post> findAllByUserOrderByCreatedAtDesc(User userFriend, Pageable pageable);

    Page<Post> findAllByUserAndSharedIsTrueOrderByCreatedAtDesc(User userNotFriend, Pageable pageable);

    Page<Post> findAllBySharedTrueOrderByCommentsDesc(Pageable pageable);

    Page<Post> findAllBySharedTrueOrderByCreatedAtDesc(Pageable pageable);


}
