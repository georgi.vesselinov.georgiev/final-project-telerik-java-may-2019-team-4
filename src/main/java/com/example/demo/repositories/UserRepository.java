package com.example.demo.repositories;

import com.example.demo.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

//    @Query("select u from User u where (:firstName is null or u.firstName = :firstName) and (:lastName is null or u.lastName = :lastName) and (:email is null"
//            + " or u.email = :email)")
//    Optional<User> findUserByFirstNameAndLastNameAndEmailAndEnabledTrue(@Param("firstName")String firstName, @Param("lastName") String lastName, @Param("email") String email);

//    Optional<User> findByFirstNameOrLastNameOrEmailAndEnabledTrue(String searchCriteria);


    Page<User> findAllByEnabledTrueAndFirstNameLike(String fistName, Pageable pageable);

    Optional<User> findByFirstNameAndEnabledTrue(String firstName);

    Optional<User> findByLastNameAndEnabledTrue(String lastName);

    Optional<User> findByEmailAndEnabledTrue(String email);

    List<User> findAllByEnabledTrue();

    Optional<User> findByEmailAndEnabledFalse(String email);

   Page<User> findAllByEnabledTrueAndSharedTrue(Pageable pageable);

    Optional<User> findByIdAndEnabledTrue(int userId);

    User findByEmail(String email);

    List<User> findAll();

    Page<User> findAll(Pageable pageable);

    boolean existsByEmail(String email);

    @Modifying
    @Query("update User u set u.password=:password where u.id=:id")
    void updatePassword(@Param("password") String password, @Param("id") int id);

}
