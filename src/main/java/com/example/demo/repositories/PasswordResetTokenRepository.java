package com.example.demo.repositories;

import com.example.demo.models.PasswordResetToken;
import com.example.demo.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken ,Integer> {

    PasswordResetToken findByPasswordResetToken(String token);
    PasswordResetToken findByUser(User user);

}
