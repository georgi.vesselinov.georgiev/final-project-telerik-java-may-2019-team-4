package com.example.demo.repositories;

import com.example.demo.models.ConfirmationToken;
import com.example.demo.models.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
    ConfirmationToken findByUser(User user);

}
