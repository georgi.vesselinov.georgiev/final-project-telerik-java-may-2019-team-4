package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
public class RequestEntityTooLargeException extends RuntimeException {
    public RequestEntityTooLargeException(String message) {
        super(message);
    }
}
