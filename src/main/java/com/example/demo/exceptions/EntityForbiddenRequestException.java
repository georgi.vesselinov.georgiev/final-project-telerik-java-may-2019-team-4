package com.example.demo.exceptions;

//@ResponseStatus(HttpStatus.FORBIDDEN)
public class EntityForbiddenRequestException extends RuntimeException{
    public EntityForbiddenRequestException(String message) {
        super(message);
    }
}
