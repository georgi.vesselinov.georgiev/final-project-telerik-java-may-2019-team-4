package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.GONE)
public class EntityAlreadyDisabledException extends RuntimeException {
    public EntityAlreadyDisabledException(String message) {
        super(message);
    }
}
