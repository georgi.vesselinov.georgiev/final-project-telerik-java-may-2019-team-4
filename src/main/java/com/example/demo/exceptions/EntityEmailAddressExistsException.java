package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.MULTIPLE_CHOICES)
public class EntityEmailAddressExistsException extends RuntimeException {
    public EntityEmailAddressExistsException(String message) {
        super(message);
    }
}
