package com.example.demo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class EntityAlreadyChangedException extends RuntimeException {
    public EntityAlreadyChangedException(String message) {
        super(message);
    }
}
