use social_network;

create table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(25) null
);

create table users
(
    user_id     int auto_increment
        primary key,
    first_name  varchar(20)       null,
    last_name   varchar(20)       null,
    email       varchar(50)       not null,
    password    varchar(255)      null,
    enabled     tinyint           not null,
    nationality varchar(50)       null,
    city        varchar(50)       null,
    photo       mediumblob        null,
    shared      tinyint default 0 not null,
    constraint username
        unique (email)
);


create table friend_requests
(
    friend_request_id int auto_increment
        primary key,
    user_sender_id    int not null,
    user_receiver_id  int not null,
    constraint sent_requests_users_user_id_fk
        foreign key (user_sender_id) references users (user_id),
    constraint sent_requests_users_user_id_fk_2
        foreign key (user_receiver_id) references users (user_id)
);

create table friendships
(
    friendship_id   int auto_increment
        primary key,
    user_friend1_id int not null,
    user_friend2_id int not null,
    constraint friendships_users_user_id_fk
        foreign key (user_friend1_id) references users (user_id),
    constraint friendships_users_user_id_fk_2
        foreign key (user_friend2_id) references users (user_id)
);

create table confirmationtoken
(
    token_id           int auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_date       datetime     null,
    user_id            int          null,
    expire_date        datetime     null,
    constraint confirmationtoken_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table password_reset_token
(
    token_id    int auto_increment
        primary key,
    token       varchar(255) default '' not null,
    create_date datetime                not null,
    user_id     int          default 0  not null,
    expire_date datetime                null,
    constraint FK_password_reset_token_users
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id    int auto_increment
        primary key,
    user_id    int               not null,
    post_text  text              not null,
    post_photo mediumblob        null,
    shared     tinyint default 0 not null,
    created_at datetime          not null,
    updated_at datetime          not null,
    constraint posts_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table comments
(
    comment_id   int auto_increment
        primary key,
    comment_text text     not null,
    post_id      int      not null,
    user_id      int      not null,
    created_at   datetime not null,
    updated_at   datetime not null,
    constraint comments_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint comments_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table comment_likes
(
    comment_id int not null,
    user_id    int not null,
    constraint comment_likes_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint comment_likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table post_likes
(
    like_id int auto_increment
        primary key,
    post_id int not null,
    user_id int not null,
    constraint likes_posts_post_id_fk
        foreign key (post_id) references posts (post_id),
    constraint likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table replies
(
    reply_id   int auto_increment
        primary key,
    reply_text text     not null,
    comment_id int      null,
    user_id    int      not null,
    created_at datetime not null,
    updated_at datetime not null,
    constraint replies_comments_comment_id_fk
        foreign key (comment_id) references comments (comment_id),
    constraint replies_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table reply_likes
(
    reply_id int not null,
    user_id  int not null,
    constraint reply_likes_replies_reply_id_fk
        foreign key (reply_id) references replies (reply_id),
    constraint reply_likes_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table user_roles
(
    user_id int null,
    role_id int null,
    constraint user_roles_roles_role_id_fk
        foreign key (role_id) references roles (role_id),
    constraint user_roles_users_user_id_fk
        foreign key (user_id) references users (user_id)
);


CREATE USER 'socialnetwork'@'localhost' IDENTIFIED BY 'pass';
GRANT USAGE ON *.* TO 'socialnetwork'@'localhost';
GRANT ALL PRIVILEGES ON `social_network`.* TO 'socialnetwork'@'localhost';
FLUSH PRIVILEGES;